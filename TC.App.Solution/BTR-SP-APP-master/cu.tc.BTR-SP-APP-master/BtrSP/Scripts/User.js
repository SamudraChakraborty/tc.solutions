﻿var BTR = BTR || {};
BTR.User = Object.create(BTR.ApiConnector);  //inherit from the ApiConnector object;
    

    BTR.User.ListBaseApiUrl = "/api/user/";

    BTR.User.Current = {};
    BTR.User.Current.uni_code = '';
    BTR.User.Current.uni_key = 0;
    BTR.User.Current.lastName = '';
    BTR.User.Current.firstName = '';
    BTR.User.Current.email = '';
    BTR.User.Current.dept_code = '';
    BTR.User.Current.upn = '';
    BTR.User.Current.role = '';


//@Function InfoRecord
//@Description: Function used to to retrieve a user record based on UPN from SharePoint

    BTR.User.InfoRecord = function (userUPN, scb, fcb) {
        var dataError = this.ErrorObject();
        if ((userUPN == null) || (userUPN.length == 0)) {
            dataError.ErrorCode = "USER_JS_P01";
            dataError.StatusText = "User UPN can not be blank";
            dataError.Message = dataError.StatusText;
            fcb(dataError);
        }

        var filter = 'upn=' + userUPN;
        this.ExecuteApiGet('byUPN', filter, scb, fcb);
	};

    
//@Function Initialize
//@Description: Function used to to set the context information for the current user

    BTR.User.GetCurrent = function (userUPN, scb, fcb) {
        if (BTR.User.Current.uni_key != 0) 
        {
            scb(BTR.User.Current);
        }
        else {
            BTR.User.InfoRecord(userUPN,
                function (userData) {
                    if ((userData == null) || (userData.uni_code == null))
                    {
                        var dataError = BTR.User.ErrorObject();
                        dataError.ErrorCode = "USER_JS_P02";
                        dataError.StatusText = "User record is blank.";
                        dataError.Message = dataError.StatusText;

                        fcb(dataError);
                    }
                    if (userData) {
                        BTR.User.Current = userData;
                    }
                    scb(BTR.User.Current);
                    return;
                }
                , function error(data) {
                    console.error(arguments.callee.name + ':' + err);
                    var errObj = BTR.User.CreateErrMsg('USER_JS_API_USER', err, arguments.callee.name + ' - ' + err);
                    fcb(errObj);
                }
            );
        }
    };

    BTR.User.UserField = function ()
    {
        if ((BTR.User.Current != null) && (BTR.User.Current.uni_key != 0))
        {
            var role = (BTR.User.Current.role == null) ? 'none' : BTR.User.Current.role.toLowerCase();
            var role_title;
            if (role != "none")  {
                switch (role) {
                    case "s":
                        role_title = "Submitter";
                        break;
                    case "rp":
                        role_title = "Responsible Person";
                        break;
                    case "bo":
                        role_title = "Budget Office";
                        break;
                    case "g":
                        role_title = "Grant";
                        break;
                }
            }
            return BTR.User.Current.uni_code + '(' + role_title + ')';
        }
        else {
            return GlobalConfig.BTR.ErrorCodes.user_not_defined;
        }
    };
