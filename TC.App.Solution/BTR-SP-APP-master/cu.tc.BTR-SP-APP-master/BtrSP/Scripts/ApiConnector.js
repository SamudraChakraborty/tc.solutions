﻿
var BTR = BTR || {};
BTR.ApiConnector = {};
BTR.ApiConnector._siteUrl = null;
BTR.ApiConnector._requestDigest = null;

//@Function SiteUrl
//@Description: Function to set the domain name for the Url (xyx.myservice.com)

BTR.ApiConnector.SiteUrl = function (hostDomain) {
    this._siteUrl = hostDomain;
};

//@Function HttpHeaders
//@Description: Function used to to build http header collection for ajax calls

BTR.ApiConnector.HttpHeaders = {
    Build: function (xMethod, xRequestDigest, eTag) {
        var baseHeaders = {
            "Accept": "application/json; odata=verbose",
            "content-type": "application/json;odata=verbose"
        };

        if (typeof xMethod !== 'undefined') {
            baseHeaders["X-HTTP-Method"] = xMethod;
        }
        if (typeof xRequestDigest !== 'undefined') {
            baseHeaders["X-RequestDigest"] = xRequestDigest;
        }

        if (typeof eTag !== 'undefined') {
            baseHeaders["IF-MATCH"] = eTag;
        }
        return baseHeaders;
    }
}

//@Function RequestDigest
//@Description: Function used to to set the request digest for SharePoint list operations

BTR.ApiConnector.RequestDigest = function (requestDigest) {
    this._requestDigest = requestDigest;
};

//@Function CreateErrMsg
//@Description: Function used create an error object

BTR.ApiConnector.CreateErrMsg = function (errorCode,data, message) {
    var dataError = ErrorObject();

    dataError.ErrorCode = errorCode;
    dataError.InnerError = '';
    
    if (data !== null) {
        dataError.InnerError = JSON.stringify(data.responseText);
        dataError.HttpStatus = data.Status;
    }
    dataError.Message = (message !== null) ? message : 'Error processing reqest';
    return dataError;
};

//@Function Error Object
//@Description: data structure for returning errors

BTR.ApiConnector.ErrorObject = function () {
    var dataError = {};
    dataError.StatusText = '';
    dataError.HttpStatus = '000';
    dataError.ErrorCode = 'GE001'; //General error 001
    dataError.InnerError = '';
    dataError.Message = '';
    dataError.Url = '';
    return dataError;
}


//@Function FullApiUrl
//@Description: Function used to to build the url for the API

BTR.ApiConnector.FullApiUri = function (uriAction) {
    if ((typeof this._siteUrl) === 'undefined') {
        throw "Site Url is not defined for Budget Transfer class";
    }
    if (typeof uriAction !== 'undefined') {
        return this._siteUrl + this.ListBaseApiUrl + uriAction;
    }
    throw "Malformed site Url {"+this._siteUrl+"} for controller, no action defined.";
}



//@Function ExecuteApiGet
//@Description: Function used to call the web api to retieve the data
BTR.ApiConnector.ExecuteApiGet = function (controllerActionName, filter, scb, fcb) {

    var ajaxUrl;
    var dataError = this.ErrorObject();

    try {
        ajaxUrl = this.FullApiUri(controllerActionName);
    }
    catch (err) {
        dataError.StatusText = "Web API site URL is has not been initialized";
        dataError.HttpStatus = "000";
        dataError.Url = '';
        dataError.Message = err.message;
        fcb(dataError);
        return;
    }

    var queryString = "";
    if ((filter != null) && (filter.length > 0)) {
        //TODO: check to see if the filter starts with a question mark
        queryString = "?" + filter;
    }

    var executeUrl = ajaxUrl + queryString;
    $.ajax({
        url: executeUrl,
        method: "GET",
        headers: this.HttpHeaders.Build(),
        success: function (data) {

            if (data == null) return scb(data);
            if (data.constructor == Array) {
                scb(data.slice(0));
            }
            else {
                scb(data);
            }
        },
        error: function (data) {
            var dataError = {};
            dataError.StatusText = data.statusText;
            dataError.HttpStatus = data.status;
            dataError.Url = executeUrl;
            dataError.Message = "(" + data.status + ") - URL(" + executeUrl + ") - " + data.statusText;
            fcb(dataError);
        }
    });
};

//@Function ExecuteApiPost
//@Description: Function used to call the web api to retieve the data

BTR.ApiConnector.ExecuteApiPost = function (controllerActionName, jsonData, filter, scb, fcb) {

    var ajaxUrl;
    var dataError = this.ErrorObject();

    try {
        ajaxUrl = this.FullApiUri(controllerActionName);
    }
    catch (err) {
        dataError.StatusText = "Web API site URL is has not been initialized";
        dataError.HttpStatus = "000";
        dataError.Url = '';
        dataError.Message = err.message;
        fcb(dataError);
        return;
    }

    var queryString = "";
    if ((filter != null) && (filter.length > 0)) {
        //TODO: check to see if the filter starts with a question mark
        queryString = "?" + filter;
    }

    var executeUrl = ajaxUrl + queryString;

    $.ajax({
        url: executeUrl,
        method: "POST",
        headers: this.HttpHeaders.Build(null, $("#__REQUESTDIGEST").val()),
        data: JSON.stringify(jsonData),
        success: function (data) {

            if (data.constructor === Array) {
                scb(data.slice(0));
            }
            else {
                scb(data);
            }
        },
        error: function (data) {
            
            var dataError = {};
            dataError.StatusText = data.statusText;
            dataError.HttpStatus = data.status;
            dataError.Url = executeUrl;
            dataError.Message = "(" + data.status + ") - URL(" + executeUrl + ") - " + data.statusText;
            console.log(JSON.stringify(data));
            fcb(dataError);
        }
    });
};

//@Function ExecuteApiDelete 
//@Description: Function used to call the web api to retieve the data

BTR.ApiConnector.ExecuteApiDelete = function (controllerActionName, filter, scb, fcb) {

    var ajaxUrl;
    var dataError = this.ErrorObject();

    try {
        ajaxUrl = this.FullApiUri(controllerActionName);
    }
    catch (err) {
        dataError.StatusText = "Web API site URL is has not been initialized";
        dataError.HttpStatus = "000";
        dataError.Url = '';
        dataError.Message = err.message;
        fcb(dataError);
        return;
    }

    var queryString = "";
    if ((filter != null) && (filter.length > 0)) {
        //TODO: check to see if the filter starts with a question mark
        queryString = "?" + filter;
    }

    var executeUrl = ajaxUrl + queryString;
    $.ajax({
        url: executeUrl,
        method: "DELETE",
        headers: this.HttpHeaders.Build(),
        success: function (data) {

            if (data.constructor === Array) {
                scb(data.slice(0));
            }
            else {
                scb(data);
            }
        },
        error: function (data) {
            var dataError = {};
            dataError.StatusText = data.statusText;
            dataError.HttpStatus = data.status;
            dataError.Url = executeUrl;
            dataError.Message = "(" + data.status + ") - URL(" + executeUrl + ") - " + data.statusText;
            fcb(dataError);
        }
    });
};


//@Function ExecuteApiPost
//@Description: Function used to call the web api to retieve the data

BTR.ApiConnector.ExecuteApiPut = function (controllerActionName, jsonData, filter, scb, fcb) {

    var ajaxUrl;
    var dataError = this.ErrorObject();

    try {
        ajaxUrl = this.FullApiUri(controllerActionName);
    }
    catch (err) {
        dataError.StatusText = "Web API site URL is has not been initialized";
        dataError.HttpStatus = "000";
        dataError.Url = '';
        dataError.Message = err.message;
        fcb(dataError);
        return;
    }

    var queryString = "";
    if ((filter != null) && (filter.length > 0)) {
        //TODO: check to see if the filter starts with a question mark
        queryString = "?" + filter;
    }

    var executeUrl = ajaxUrl + queryString;

    $.ajax({
        url: executeUrl,
        method: "PUT",
        headers: this.HttpHeaders.Build(null, null),
        data: JSON.stringify(jsonData),
        success: function (data) {

            if (data.constructor === Array) {
                scb(data.slice(0));
            }
            else {
                scb(data);
            }
        },
        error: function (data) {

            var dataError = {};
            dataError.StatusText = data.statusText;
            dataError.HttpStatus = data.status;
            dataError.Url = executeUrl;
            dataError.Message = "(" + data.status + ") - URL(" + executeUrl + ") - " + data.statusText;
            console.log(JSON.stringify(data));
            fcb(dataError);
        }
    });
};
