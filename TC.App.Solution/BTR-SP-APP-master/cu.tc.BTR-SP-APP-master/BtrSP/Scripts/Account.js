﻿var BTR = BTR || {};

BTR.Account = Object.create(BTR.ApiConnector);  //inherit from the ApiConnector object
BTR.Account.WebApiUrlAccounts = "Accounts";
BTR.Account.WebApiUrlAccountsByIndexKey = "AccountsByIndexKey";
BTR.Account.WebApiUrlAccountBalance = "AccountBalance";	
BTR.Account.WebApiUrlAccountBalances = "AccountBalances";	
BTR.Account.ListBaseApiUrl = "/api/accounts/";


//--------------------------------------------------------- Read  Item  ---------------------------------------------------------
//@Function GetIndicesOwnedByDept 
//@Description: Get all the indices 
BTR.Account.GetAccounts = function (scb, fcb) {
    try {
        this.ExecuteApiGet(BTR.Account.WebApiUrlIndices, null, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('ACCOUNT_JS_API_ACCOUNTS', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};


//@Function GetIndicesOwnedByDept 
//@Description: Get all the indices owned by the user and filtered by department, if dept_code is not supplied then results will not be filtered by department
//@Parameters: uni = Required, dept_code = optional
BTR.Account.GetAccountsByIndexKey = function (index_key, scb, fcb) {
    var dataError = this.ErrorObject();

    if ((index_key == null) || (index_key.length == 0)) {
        dataError.ErrorCode = "ACCOUNT_JS_P01";
        dataError.StatusText = "Index key can not be blank";
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }
    try {
        var filter = 'index_key=' + index_key;
        this.ExecuteApiGet(BTR.Account.WebApiUrlAccountsByIndexKey, filter, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('ACCOUNT_JS_API_BYINDEXKEY', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};

//TODO: Review this function it is not right!!!
//@Function GetAccountsByIndexKeys 
//@Description: Get all the indices filtered by department
BTR.Account.GetAccountsByIndexKeys = function (dept_codes, scb, fcb) {

    var dataError = this.ErrorObject();

    if ((index_key == null) || (index_key.length == 0)) {
        dataError.ErrorCode = "ACCOUNT_JS_P02";
        dataError.StatusText = "Index key can not be blank";
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        var filter = 'index_key=' + index_key;
        this.ExecuteApiGet(BTR.Account.WebApiUrlAccountsByIndexKey, filter, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('ACCOUNT_JS_API_BYINDEXKYES', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};

//@Function GetAccountBalance 
//@Description: Get all the indices filtered by department
BTR.Account.GetAccountBalance = function (account_key, scb, fcb) {

    var dataError = this.ErrorObject();

    if ((account_key == null) || (account_key.length == 0)) {
        dataError.ErrorCode = "ACCOUNT_JS_P01";
        dataError.StatusText = "Index key can not be blank";
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }
    try {
        var filter = 'account_key=' + account_key;
        this.ExecuteApiGet(BTR.Account.WebApiUrlAccountBalance, filter, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('ACCOUNT_JS_API_ACCOUNTS', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};

//@Function GetAccountBalances
//@Description: Get all accounts based on a set of index_keys
BTR.Account.GetAccountBalances = function (index_keys, scb, fcb) {

    var dataError = this.ErrorObject();

    if ((index_keys == null) || (index_keys.length == 0)) {
        dataError.ErrorCode = "ACCOUNT_JS_P01";
        dataError.StatusText = "Index key can not be blank";
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }
    try {
        var filter = 'index_keys=' + index_keys;
        this.ExecuteApiGet(BTR.Account.WebApiUrlAccountBalances, filter, scb, fcb);

    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('ACCOUNT_JS_API_BALANCES', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};