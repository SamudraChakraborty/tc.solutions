﻿var UI = UI || {};
UI.Wizard = {};

UI.Wizard.Pages = null;
UI.Wizard.CurrentPage = null;
UI.Wizard.PagesMaximum = null;
UI.Wizard.PreValidationFunctions = null;

UI.Wizard.Initialize = function () {
    if ($('#wizardPages').val() !== null) {
        this.Pages = $('#wizardPages').val().split(';');
        $('#wizardPagesCurrentPage').val(this.Pages[0]);
        this.PagesMaximum = this.Pages[this.Pages.length - 1]; //pages need to be in order
        $("[wizardpage]").css('display', 'none');
        $($("[wizardpage]")[$('#wizardPagesCurrentPage').val() - 1]).css('display', 'block');
        $('#previous-button').hide();
    }
};

UI.Wizard.AddValidation = function (activiationPage, validationFunction) {
    if (this.PreValidationFunctions == null)
    {
        this.PreValidationFunctions = new Array();
    }
    var validation = { 'activiationPage': 0, 'validationFunction': null };
    validation.activiationPage = activiationPage;
    validation.validationFunction = validationFunction;

    this.PreValidationFunctions.push(validation);
};

UI.Wizard.CheckValidation = function (currentPage) {
    //debugger;

    if (this.PreValidationFunctions == null)
    {
        return true;
    }

    for (var index = 0; index < this.PreValidationFunctions.length; index++)
    {
        if ((this.PreValidationFunctions[index].activiationPage == currentPage) && ((this.PreValidationFunctions[index].validationFunction(currentPage) == false)))
        {
            return false;
        }
    }
    return true;
}

UI.Wizard.NextStep = function ()
{
    var currentStep = Number($('#wizardPagesCurrentPage').val()); 

    if (this.CheckValidation(currentStep) == false) {
        return;
    }
	
	var nextStep = currentStep + ((currentStep < this.PagesMaximum) ? 1 : 0); //convert to a number
	var indexCurrent = currentStep - 1; //for the array index
	var indexNext = nextStep -1; 
	
	$($('.stepPreviewPanal li')[indexCurrent]).removeClass('active');
	$($('.stepPreviewPanal li')[indexCurrent]).addClass('disabled');
	$($('.stepPreviewPanal li')[indexNext]).removeClass('disabled');
	$($('.stepPreviewPanal li')[indexNext]).addClass('active');

	$($("[wizardpage]")[indexCurrent]).css('display','none');
	$($("[wizardpage]")[indexNext]).css('display','block');
	if (currentStep == 1)	
	{
		$('#previous-button').show();
	}
	if (nextStep == this.PagesMaximum)
	{
		$('#next-button').hide();
	}
	$('#wizardPagesCurrentPage').val(nextStep);
}

UI.Wizard.PreviousStep = function()
{
	var currentStep = Number($('#wizardPagesCurrentPage').val());
    var prevStep = currentStep - ((currentStep > 1) ? 1 : 0); //convert to a number
	var indexCurrent = currentStep - 1; //for the array index
	var indexPrevious = prevStep -1; 

	$($('.stepPreviewPanal li')[indexCurrent]).removeClass('active');
	$($('.stepPreviewPanal li')[indexCurrent]).addClass('disabled');
	$($('.stepPreviewPanal li')[indexPrevious]).removeClass('disabled');
	$($('.stepPreviewPanal li')[indexPrevious]).addClass('active');
	
	$($("[wizardpage]")[indexCurrent]).css('display','none');
	$($("[wizardpage]")[indexPrevious]).css('display','block');
	if (prevStep == 1)	
	{
		$('#previous-button').hide();
	}
	if (currentStep == this.PagesMaximum)
	{
		$('#next-button').show();
	}

	$('#wizardPagesCurrentPage').val(prevStep );	
}

UI.Wizard.GoToPage = function GoToWizardPage(pageNumber)
{
	var currentStep = Number($('#wizardPagesCurrentPage').val());
    var gotoStep = pageNumber; //convert to a number
    var indexCurrent = currentStep - 1; //for the array index
    var indexGoto = gotoStep - 1; 

    if (gotoStep > currentStep)
    {
        if (this.CheckValidation(currentStep) == false) {
            return;
        }
    }

	$($('.stepPreviewPanal li')[indexCurrent]).removeClass('active');
	$($('.stepPreviewPanal li')[indexCurrent]).addClass('disabled');
    $($('.stepPreviewPanal li')[indexGoto]).removeClass('disabled');
    $($('.stepPreviewPanal li')[indexGoto]).addClass('active');
	
	$($("[wizardpage]")[indexCurrent]).css('display','none');
    $($("[wizardpage]")[indexGoto]).css('display','block');
	if (gotoStep == 1)	
	{
		$('#previous-button').hide();
    }
    else
    {
        $('#previous-button').show();
    }

	if (gotoStep == this.PagesMaximum)
	{
		$('#next-button').hide();
    }
    else
    {
        $('#next-button').show();
    }

	$('#wizardPagesCurrentPage').val(gotoStep);	
}