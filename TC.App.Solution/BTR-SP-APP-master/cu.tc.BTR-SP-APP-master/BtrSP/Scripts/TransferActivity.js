﻿var BTR = BTR || {};
BTR.TransferActivity = {};
BTR.TransferActivity = Object.create(BTR.ApiConnector);  //inherit from the ApiConnector object


BTR.TransferActivity.ListBaseApiUrl = "/api/transferactivity/";

//reinserted because of data dependencies
BTR.TransferActivity.DataItem = {
    Create: function () {
        var jsonData = {};
        return jsonData;
    }
};


//@Function Item
//@Description: Function used to to return a transfer activity by ID

BTR.TransferActivity.Item = function (item_key, scb, fcb) {
    if ((item_key == null) || (item_key.length == 0)) {
        dataError.ErrorCode = "TA_JS_P01";
        dataError.StatusText = "Parameter item_key can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        this.ExecuteApiGet('item', 'transfer_activity_key=' + item_key, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('TA_JS_API_ITEM', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};

//@Function GetBTRAssociatedTransferActivities
//@Description: Function used to to return transfer activites associated with a BTR record

BTR.TransferActivity.GetBTRAssociatedTransferActivities = function (btr_key,scb,fcb)
{
    var filter = "btr_key=" + btr_key;
    if ((btr_key == null) || (btr_key.length == 0)) {
        dataError.ErrorCode = "TA_JS_P02";
        dataError.StatusText = "Parameter btr_key can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        this.ExecuteApiGet('ItemsByBtrId', filter, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('TA_JS_API_ITEM', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};

//@Function ResponsiblePerson
//@Description: Function used to to return transfer activity for responsible person

BTR.TransferActivity.ResponsiblePerson = function (btr_key, scb, fcb) {

    if ((btr_key == null) || (btr_key.length == 0)) {
        dataError.ErrorCode = "TA_JS_P03";
        dataError.StatusText = "Parameter btr_key can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    var filter = "btr_key=" + btr_key;
    
    try {
        this.ExecuteApiGet('ResponsiblePersons', filter, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('TA_JS_API_ITEM', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};

//@Function ReviewerOwned
//@Description: Function used to to return transfer activity owned by reviewers

BTR.TransferActivity.ReviewerOwned = function (uni_key, scb, fcb) {
    if ((btr_key == null) || (btr_key.length == 0)) {
        dataError.ErrorCode = "TA_JS_P04";
        dataError.StatusText = "Parameter btr_key can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    var filter = "btr_key=" + btr_key;
    try {
        this.ExecuteApiGet('ReviewerOwned', filter, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('TA_JS_API_ITEM', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
    
};

//@Function Create
//@Description: Function used to to create a transaction activity

BTR.TransferActivity.Create = function(jsonData,scb,fcb)
{
    if (jsonData == null)  {
        dataError.ErrorCode = "TA_JS_P05";
        dataError.StatusText = "transfer activity parameter is blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        this.ExecuteApiPost('create', jsonData, null, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('TA_JS_API_ITEM', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};

//@Function Update
//@Description: Function used to to update the transactivity

BTR.TransferActivity.Update = function(listItemId,jsonData,eTag,scb,fcb)
{
    if (jsonData == null) {
        dataError.ErrorCode = "TA_JS_P05";
        dataError.StatusText = "transfer activity parameter is blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }
    try {
        this.ExecuteApiPost('update', jsonData, null, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('TA_JS_API_ITEM', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);

    }
};

    //@Function Update
    //@Description: Function used to to update the transactivity
BTR.TransferActivity.Delete = function(transferactivity_key,scb,fcb)
{
    if ((transferactivity_key == null) || (transferactivity_key.length == 0)) {
        dataError.ErrorCode = "TA_JS_P04";
        dataError.StatusText = "Parameter transferactivity_key can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        this.ExecuteApiDelete('delete', jsonData, null, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('TA_JS_API_DELETE', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
    
};

//@Function BatchSave
//@Description: Function used to to save multiple transfer activities at once
BTR.TransferActivity.BatchSave = function (jsonData, scb, fcb) {
    if (jsonData == null) {
        dataError.ErrorCode = "TA_JS_P05";
        dataError.StatusText = "transfer activity parameter is blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }
    try {
        this.ExecuteApiPost('batchsave', jsonData, null, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('TA_JS_API_BATCHSAVE', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);

    }
};

//@Function UpdateStatus (Not working needs to be converted)
//@Description: Function used to to update the transactivity status
BTR.TransferActivity.UpdateStatus = function (item_key, action) {
    var vActionValue = (action == 1) ? "Approved" : "Rejected";

    if ((transferactivity_key == null) || (transferactivity_key.length == 0)) {
        dataError.ErrorCode = "TA_JS_P05";
        dataError.StatusText = "transfer activity parameter is blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        jsonData['transfer_activity_key'] = item_key;
        jsonData['approval_status'] = vActionValue;
        this.ExecuteApiPut('updatestatus', jsonData, null, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('TA_JS_API_UDATESTATUS', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};	