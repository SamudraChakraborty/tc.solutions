﻿var global_error_background = '#ffaaee';

function TitleAnchorToggle(liElement)
{
    //if ($(liElement).hasClass('active')) return;
   // $(liElement).toggleClass('active');
}

function reloadForm() {
	if ((_gForm_Saved_BTR == 1) && (_gForm_Saved_TransferActivities == 1))
    {
        window.location.href = 'BTR.aspx';
		//window.location.href = window.location.href + "&rID=" +	_gForm_ID;
	}
	if ((_gForm_Saved_BTR == 2) && (_gForm_Saved_TransferActivities == 2))
	{
		window.location.href = 'BTR.aspx';
	}
}
	
function DeleteTransferActivity(position,rowId) {
		
	//if this is not a new entry, set action field to delete  else delete the html element should use an operation inside the data grid 
	//check action field
	//fromTAItemAction
	var vPosition = position.toLowerCase();
	var action = $('#' + vPosition + 'TAItemAction' + rowId).val();
	var itemCount = 0;
		
	if (vPosition == 'from') {
		itemCount = _gRptSect_From.GetRowIds().length;
	}
	else {
		itemCount = _gRptSect_To.GetRowIds().length;
	}
		
	if (itemCount == 1)
	{
		alert('This row can not be deleted.');
		return;
	}
		
	if (action.toUpperCase() == 'UPDATE') {
		//this already exist so hide the section and update the action
			
		$('#' + vPosition + 'TASection' + rowId).hide();
		$('#' + vPosition + 'TAItemAction' + rowId).val('delete');
		//need to zero out the amount field
		if (vPosition == 'from') 			{
			$('#fromTAAmountToTransfer' + rowId).val(0);
		}
		else {
			$('#toTAAmountToTransfer' + rowId).val(0);
		}
			
	}
		
	if (action.toUpperCase() == 'CREATE') {
		$('#' + vPosition + 'TASection' + rowId).remove();
		if (vPosition == 'from') {
			itemCount = _gRptSect_From.DeleteRow(rowId);
		}
		else {
			itemCount = _gRptSect_To.DeleteRow(rowId);
		}
	}
	//
    $('#totalFromValue').text(Utils.Currency.format(Formula_CalcTotals('[rel="from.select.amounttotransfer"]')));
    $('#totalToValue').text(Utils.Currency.format(Formula_CalcTotals('[rel="to.select.amounttorecieve"]')));

}
	
function ShowPositions() {
	$('#popupPositions').popup('show');

}
	
function GetAllAccountsAndBalances(aryValues,cbFunction)
{
	var index_keys = aryValues.join(",");

	BTR.Account.GetAccountBalances(index_keys,
		function (data) {
			cbFunction(data)
		},
		function (data) {
            console.error("Error(GetAllAccountsAndBalances): "+ data.Message);
		}
	);
}
	
function GetAccounts(controlId,index_key,selectedValue)
{
	BTR.Account.GetAccountsByIndexKey(index_key,	
		function (data) {
			var _select = new Array();
				
			_select.push('<option value="-1">Select Account</option>');
			$.each(data, function(index,obj) {
                _select.push('<option value="'+obj.account_key+'">'+obj.account_number_description+'</option>');
			});
			//if this is on the From side then clear balance
				
			//clear accounts and repopulate
			$(controlId).empty().append(_select.join());			
			if ((typeof selectedValue ) !== 'undefined')
			{
				$(controlId).val(selectedValue);
			}
		},
		function (data) {
            console.error("Error(GetAccountsByIndexKey): "+ data.Message);
        }
	);
}

function GetAccountBalance(controlId,account_key)
{
	BTR.Account.GetAccountBalance(account_key,	
		function (dataArray) {
            var data = dataArray[0];

            $(controlId).text(Utils.Currency.format(data.account_balance));
		},
		function (data) {
            console.error("Error(GetBTRItems): "+ data.Message);
        }
	);
}


function GetIndicesOwned(cb)
{
	var dept_code = null;
		
	if ((_gData_IndexDenorm == null) || ((typeof _gData_IndexDenorm) == 'undefined'))
	{
		if (_gForm_transfer_type == _gConstForm_transfer_type_IntraDept) {
			dept_code = BTR.User.Current.dept_code;
		}
			
        BTR.Index.GetIndicesOwnedByDept(BTR.User.Current.uni_code,dept_code,
			function (data) {
				_gData_IndexDenorm = data; // clone the array
				cb(_gData_IndexDenorm);
			},
			function (data) {
                console.error("Error (GetIndicesOwned): "+ data.Message);
			}
		);
	}
	else {
		cb(_gData_IndexDenorm);
	}
}

function GetIndicesAssigndByUni(cb) {
    var dept_code = null;

    if ((_gData_IndexDenorm == null) || ((typeof _gData_IndexDenorm) == 'undefined')) {

        BTR.Index.GetIndicesAssignedByUni(BTR.User.Current.uni_code, 
            function (data) {
                _gData_IndexDenorm = data; // clone the array
                cb(_gData_IndexDenorm);
            },
            function (data) {
                console.error("Error (GetIndicesOwned): " + data.Message);
            }
        );
    }
    else {
        cb(_gData_IndexDenorm);
    }
}


function GetIndicesAll(cb)
{
	var dept_code = null;

	if ((_gData_IndexTo == null) || ((typeof _gData_IndexTo) == 'undefined'))
	{
		if (_gForm_transfer_type == _gConstForm_transfer_type_IntraDept) {
            dept_code = BTR.User.Current.dept_code;
		}
			
		//if dept_code is null it will return all indices
		BTR.Index.GetIndicesByDept(dept_code,
			function (data) {
				_gData_IndexTo = data; // clone the array
				cb(_gData_IndexTo);
			},
			function (data) {
                console.error("Error(GetIndicesAll): "+ data.Message);
			}
		);
	}
	else {
		cb(_gData_IndexTo.slice(0));
	}
}

	
var	DataGrid = function (rootNodeElement,vSection) {
	return {
		rootNode : rootNodeElement,
		gvRows : [],
		section: vSection,
		OnAddRow:  function(cbEvent) {
			this.addRowEvent = cbEvent;
		},
		GetFooter : function() {
			return $('#'+this.section+' div[datalabel="footer"]');
		},
		DeleteRow : function(rowId) {
			for(var i = this.gvRows.length - 1; i >= 0; i--) {
				if(this.gvRows[i] == rowId) {
					this.gvRows.splice(i, 1);
				}
			}
		},
        AddRow: function (objSettings) {
            var newId = '_F' + this.gvRows.length;
            var htmlString = this.rootNode.children().children().html().replace(/_FXXXXX/g, newId);
            htmlString = htmlString.replace(/_isrequired/g, 'required'); //take care of required fields
            $(htmlString).appendTo('#' + this.section + ' tbody[datalabel="body"]');

            if (this.gvRows.length == 0) {
                var anchorId = this.section + newId;
                var addRowLink = this.GetFooter().find("a[datalabel='dlAddNewRow']");
                addRowLink.click(this.AddRow.bind(this));
            }
            this.gvRows.push(newId);
            //do databings
            //wire events
            if (typeof this.addRowEvent !== 'undefined') {
                this.addRowEvent(this.addRowEvent.bind(this));
            }
            if ((objSettings !== null) && ((typeof objSettings) !== 'undefined')) {
                for (var iCount = 0; iCount < objSettings.length; iCount++) {
                    var obj = objSettings[iCount];
                    console.log(obj);
                    console.log(obj.name);
                    console.log(obj.properties);
                    var htmlElement = $('[rel="' + obj.name + '"]');
                }
            }

            if (this.section === 'fromColumn') {
                var wholeSelector = '#fromTAAmountToTransfer' + newId;
            } else {
                var wholeSelector = '#toTAAmountToTransfer' + newId;
            }

            new Cleave(wholeSelector, {
                numeral: true,
                numeraldecimalmark: '.',
                delimiter: ','
            })

        },  //end of function
		Populate : function(elemName, GetDataFn, PopulateFn) {
			GetDataFn(PopulateFn); 
		},
		GetRowIds : function() {
			return this.gvRows;
		}, 
		GetElement : function(elemName) {
					$('[rel="'+elemName+'"]');
		} //end of function
	}; //end of return
}; // end of variable
	
function PopulateElement (GetDataFn, PopulateFn) {
	GetDataFn(PopulateFn); 
}

function SetFormStatus(status) {
	if (status !== null) status = status.toLowerCase();
	_gForm_IsDirty = (status == 'clean');
}
	
function IsFormDirty() {
	return _gForm_IsDirty;
}

function CancelForm(warning) {
	if (warning && IsFormDirty())
	{
		var userSelection = confirm("Are you sure that you want to cancel your current changes?");
		if (!userSelection) 
		{
			return;
		}
	}
	//window.location.href = "../";
	window.location.href = "BTR.aspx";
}
	
function ErrorClear()
{
	$("#divErrorPane").css('visibility','hidden');
	$("#divErrorBody").html("");
}

function ErrorDisplay()
{
	$("#divErrorPane").css('visibility','visible');
}
	
function ErrorWrite(error)
{
	$("#divErrorBody").append(error+"<p/>");
}
	
function IsFormDataValid(currentPage) {
    var errorCount = 0;
    var checkPage = 0;
    //Evaluate all Selectors
    
	ErrorClear();
    $("form *[required]").css("background-color", "white");
    var elements;
    if ((typeof currentPage) == 'undefined')
    {
        elements = $("form *[required]");
    }
    else
    {
        checkPage = currentPage;
        elements = $("form [wizardpage='"+checkPage+"'] *[required]");
    }

	for(var index = 0; index < elements.length;  index++)
	{
		element = elements[index];
		if (element.id.indexOf('_FXXXXX') > 0) continue; //skip hidden repeating section

		if (element.localName == "select")
		{
			if(element.selectedIndex == "0")
			{
				element.style.backgroundColor = global_error_background;
				++errorCount;
				ErrorWrite(errorCount + ': ERR-C02 - ('+ element.name +') please select a value.');
			}
		}
        if ((element.localName == "input") || (element.localName == "textarea"))
		{
			if(element.value == "")
			{
				element.style.backgroundColor = global_error_background;
				++errorCount;
				ErrorWrite(errorCount + ': ERR-C02 - ('+ element.name +') please enter a value.');
			}
		}
	}

    //Custom Rules
    //check the radio buttons
    if ((currentPage == 1) || (checkPage == 0)) {

        if (($("#budgetType_Permanent").is(":checked") == false) && ($("#budgetType_Temporary").is(":checked") == false)) {
            ++errorCount;
            ErrorWrite(errorCount + ': ERR-C02 - Budget Type can not be blnak, select a value.');
        }
    }

    if ((currentPage == 3) || (checkPage == 0))
    {
        //check if totalFromValue == totalToValue
        if ($("#totalFromValue").text() !== $("#totalToValue").text()) {
            ++errorCount;
            ErrorWrite(errorCount + ': ERR-C02 - <u>From</u> total value and <u>To</u> total value must be equal.');
        }
        elements = $("form [wizardpage='" + checkPage + "'] [id='fromTABalance_*']");
        for (var index = 0; index < elements.length; index++) {
            element = elements[index];
            if (element.id.indexOf('_FXXXXX') > 0) continue; //skip hidden repeating 
        }
    }

    //Checking if the transfer activity totals match 
    if ((currentPage == 3) || (checkPage == 0))
    {
        //check if totalFromValue == totalToValue
        if ($("#totalFromValue").text() !== $("#totalToValue").text()) {
            ++errorCount;
            ErrorWrite(errorCount + ': ERR-C02 - <u>From</u> total value and <u>To</u> total value must be equal.');
        }
    }
		
	if (errorCount > 0)
	{
		ErrorDisplay();
	}
	return (errorCount == 0);
}
	
function CollectFormDataBTR() {
	var frmObj = BTR.BudgetTransfer.DataItem.Create();
		
	frmObj.btr_key = _gForm_ID;
    //var budget_type = $('[rel-field="BTR.budgetType"]').val();
    
    var fiscalYear = $('#fiscalYear').val();
    frmObj.fiscal_year = fiscalYear;

    var budget_type = $('[rel-field="BTR.budgetType"]:checked').val();
	frmObj.budget_type_key = BTR.LookupValues.GetKey(BTR.LookupValues.Budget_Types.values,budget_type);
	frmObj.budget_type = budget_type;
		
	var life_cycle = $('#txtBTR_life_cycle').val();
	frmObj.life_cycle = life_cycle; 
	frmObj.life_cycle_key = BTR.LookupValues.GetKey(BTR.LookupValues.Life_Cycles.values,life_cycle);

	var transfer_type = $('#txtBTR_transfer_type').val();
	frmObj.transfer_type_key = BTR.LookupValues.GetKey(BTR.LookupValues.Transfer_Types.values,transfer_type);
	frmObj.transfer_type = transfer_type;

	frmObj.title = $('#txtBTR_title').val();
	frmObj.total_amount = Formula_CalcTotals('[rel="from.select.amounttotransfer"]');
	frmObj.requestor_uni_key = BTR.User.Current.uni_key;
	frmObj.modified_by = BTR.User.Current.uni_key; //this should equal the current user 
	frmObj.created_by = BTR.User.Current.uni_key; 
	frmObj.explanation = $('#txtexplanation').val();
	return frmObj;
}
	
function CollectTransferActivityItem(position,action,itemIndex,id,btrObj)
{
	//Create the item object with the metadata item in it
	var item = BTR.TransferActivity.DataItem.Create();
		
	var elemIdIndex =  '#' + position + 'TAIndex'+id;
	var elemIdAccount =  '#' + position + 'TAAccount'+id;
	item.transfer_activity_key = $('#' + position + 'TAListItemId'+id).val();
	//Common fields
	item.row_id = $('#' + position + 'TAItemIndex' + id).val();
	item.row_guid = '';
	item.btr_guid = btrObj.btr_guid;
	item.row_id = itemIndex;
	item.btr_key = btrObj.btr_key;
	item.position_type = position.toUpperCase();
	item.position_type_key = BTR.LookupValues.GetKey(BTR.LookupValues.Position_Types.values,position);

	//Index data
	item.index_key = $(elemIdIndex).val();

	//Account Information
	item.account_key = $(elemIdAccount).val();
		
	//Amount
	item.amount = $('#'+position+'TAAmountToTransfer'+id).val();
	item.modified_by = BTR.User.Current.uni_key; //this should equal the current user 

	if (action.toLowerCase() == "create")
	{
		item.created_by = BTR.User.Current.uni_key; //this should only be set if this is a new record but the 
	}
		
	return item;			
}
	
function CollectFormDataTransferActivities(btrObj) {
	var TransferItems = new Array();
	var rowIdArray = _gRptSect_From.GetRowIds();
    var iCnt;
    var oAction;
	for(iCnt = 0; iCnt < rowIdArray.length; iCnt++) 	{
		oAction = {};
		oAction.action = $('#fromTAItemAction' + rowIdArray[iCnt]).val();
		oAction.transfer_activity= CollectTransferActivityItem('from',oAction.action,iCnt,rowIdArray[iCnt],btrObj);

		TransferItems.push(oAction);
	}
	
	rowIdArray = _gRptSect_To.GetRowIds();
	for(iCnt = 0; iCnt < rowIdArray.length; iCnt++) 	{
		oAction= {};
		oAction.action = $('#toTAItemAction' + rowIdArray[iCnt]).val();
		oAction.transfer_activity= CollectTransferActivityItem('to',oAction.action,iCnt,rowIdArray[iCnt],btrObj);
		TransferItems.push(oAction);
	}
	return TransferItems;
}
	
function SaveTransferActivities(aryTransferActivityObjItems,scb,fcb) {
	BTR.TransferActivity.BatchSave(aryTransferActivityObjItems,scb, function(errObj)
	{
		console.log(errObj);
        console.error('Error saving data'); 
	});
}

function FormSavedUpdates() 	{
	$("#divStatus"+ _gBtrObj.life_cycle).css('background-color','red').css('color','white');
	SetFormStatus('clean');
	$("#btnSubmit").css('display','inline');
	$("#modalStatustitle").text('Data saved');
	$('#loadingMessagePassed').toggle();
	$('#loadingIcon').toggle();
}

function FormSavedWithError(errorMessage) 	{
	$("#divStatus"+ _gBtrObj.life_cycle).css('background-color','red').css('color','white');
	SetFormStatus('dirty');
	$("#modalStatustitle").text('Error saving data');
	$('#loadingMessageFailed').toggle();
	$('#loadingMessageText').val('Error saving data:'+errorMessage);		
    $('#loadingMessageText').toggle();	
	$('#loadingIcon').toggle();
}

function SubmitToBanner(btr_key) {
	Banner.JV.Create(btr_key,
	function () {
		//successfully submitted to banner
			
	},
	function () {
//			failed to submit to banner
	});
}


function SaveForm(saveMode) {
	var isDraftMode = (saveMode.toLowerCase() == 'draft');
	var saveTypeDraft = (saveMode.toLowerCase() == 'draft');
	var isSaveTypeDraft = (saveMode.toLowerCase() == 'draft');
	var isSaveTypeSubmit = !isSaveTypeDraft;
	var eTag = "*";
	var done = false;
	var aryTransferActivityObjItems;
		

		
	if (IsFormDataValid()) {
		_gForm_Saved_BTR = 0;
		_gForm_Saved_TransferActivities = 0;

		//TODO: Check if form has been submitted already
		if (isSaveTypeSubmit) {
			var userSelection = confirm("Are you sure that you want to submit your changes for approval?");
			if (!userSelection) 
			{
				return;
			}
		}

		$("#modalStatustitle").text('Saving data');
			
		if ($('#loadingIcon').css('display') !== 'block')
		{
			$('#loadingMessage').toggle();
			$('#loadingIcon').toggle();
	
		}
		$('#modalStatus').modal();
			
		//Preprocessing before information is collected
			
		if (isDraftMode) {
		
			$('#txtBTR_life_cycle').val('Draft');
		}
		else
		{
			$('#txtBTR_life_cycle').val('Approval'); 
		}
			

		var formDataBTR = CollectFormDataBTR();
		if (_gForm_Mode == _gConst_Form_ModeNew) {
			BTR.BudgetTransfer.Create(formDataBTR,
			function(data) {
				//write the data back into its source
				_gForm_Mode = _gConst_Form_ModeEdit; //put the form into 
				_gForm_ID = data.btr_key;
				PopulateBTRObject(data);
				$(".statusDivText").css('background-color','white').css('color','black');
				$("#divStatus"+ _gBtrObj.life_cycle).css('background-color','red').css('color','white');
				aryTransferActivityObjItems = CollectFormDataTransferActivities(_gBtrObj);
				_gForm_Saved_BTR = (saveMode.toLowerCase() == 'draft') ? 1 : 2;
				SaveTransferActivities(aryTransferActivityObjItems, 
					function() {
                        _gForm_Mode = _gConst_Form_ModeEdit; //put the form into edit mode
                        FileUtils.UploadFiles('file-upload', _gBtrObj.btr_guid);
						FormSavedUpdates();
                        _gForm_Saved_TransferActivities = (saveMode.toLowerCase() == 'draft') ? 1 : 2;

                        
						if (isSaveTypeSubmit)
						{
                            //SubmitForApprovalWorkflow(_gForm_ID);
						}
					},
					function (errData) {
						FormSavedWithError(errData.InnerError);
					}
				);
			},
			function(errData) {
				FormSavedWithError(errData.InnerError);
			});
		}
		else {
			BTR.BudgetTransfer.Update(_gBtrObj.btr_key,formDataBTR,eTag,
				function () {
						_gForm_Saved_BTR = (saveMode.toLowerCase() == 'draft') ? 1 : 2;
						aryTransferActivityObjItems = CollectFormDataTransferActivities(_gBtrObj);
					
						SaveTransferActivities(aryTransferActivityObjItems, 
							function() {
                                _gForm_Saved_TransferActivities = (saveMode.toLowerCase() == 'draft') ? 1 : 2;
                                FileUtils.UploadFiles('file-upload', _gBtrObj.btr_guid);
								if (isSaveTypeSubmit )
								{
									//SubmitForApprovalWorkflow(_gForm_ID);
                                }
                                FormSavedUpdates();
							},
							function(erroData) {
								//an error happened
								FormSavedWithError(errData.InnerError);
							}
						);
				},
				function(errData) {
					FormSavedWithError(errData.InnerError);
				}
			);
		}
	}
	else {
		$("#modalStatustitle").text('Saving data - Validation Error');
		$('#loadingMessage').toggle();
		$('#loadingIcon').toggle();
	}
}

function SubmitForApprovalWorkflow(btr_key, scb, fcb)
{
    BTR.BudgetTransfer.ResponsiblePerson(btr_key,
        function (results) {
            var responsible_person = '';
            
            if (results.length > 2)
            {
                //display that the workflow can not be initiatied because it has two RP
                FormSavedUpdates();
                alert('This BTR can not go through a workflow because it has two Responsible Persons');
                return;
            }
            responsible_person = results[0];
            // write to
            BTR.BudgetTransfer.BudgetAdministrator(btr_key,
                function (result) {
                    var budget_admin = result[0];
                    //$pnp add an entry into the workflow list
                },
                function (err) {
                    FormSavedWithError(errData.InnerError);
                }
            );
            FormSavedUpdates();
        },
        function (errData) {
            FormSavedWithError(errData.InnerError);
        }
    );
}

//---------------- Formula Functions	
function Formula_CalcTotals(selector) {
	var elemArray = $(selector);
	var value = 0;

	$.each(elemArray, function(index, val) {
	//add up the elements
		if (val.value.length !== 0)
		{
			value += val.value.replace(/,/g, "") * 1.00;
		}
	});
	return value;
}

function PopulateTransferActivities(transferActivities,accountCache)
{
	for(var index=0; index < transferActivities.length; index++)
	{
		//populate rows
		var position = transferActivities[index].position_type.toLowerCase();
        var lastId;
        var rowArray;

		if (position == "from")
		{
			_gRptSect_From.AddRow();
			rowArray = _gRptSect_From.GetRowIds();
			lastId = rowArray[rowArray.length-1];
			$('#fromTAIndex'+lastId).val(transferActivities[index].index_key);
			//fetch and populate data
			GetAccountAndBalanceFromCache(accountCache,'#fromTAAccount'+lastId,'#fromTABalance'+lastId,transferActivities[index].index_key,transferActivities[index].account_key);

			$('#fromTAItemIndex' + lastId).val(transferActivities[index]);
			$('#fromTAAmountToTransfer'+lastId).val(transferActivities[index].amount);
			$('#fromTAItemAction'+lastId).val('update');
			$('#fromTAListItemId'+lastId).val(transferActivities[index].transfer_activity_key);
				
		}
		else
		{
			_gRptSect_To.AddRow();
			rowArray = _gRptSect_To.GetRowIds();
			lastId = rowArray[rowArray.length-1];
			$('#toTAIndex'+lastId).val(transferActivities[index].index_key);
            GetAccountAndBalanceFromCache(accountCache, '#toTAAccount' + lastId, '#toTABalance'+lastId,transferActivities[index].index_key,transferActivities[index].account_key);
				
			//fetch and populate data
			$('#toTAItemIndex' + lastId).val(transferActivities[index]);
			$('#toTAAmountToTransfer'+lastId).val(transferActivities[index].amount);
			$('#toTAItemAction'+lastId).val('update');
			$('#toTAListItemId'+lastId).val(transferActivities[index].transfer_activity_key);
		}
	} //end of for loop
}

//Populate the global BTR Object variable
function PopulateBTRObject(oItem) {
	//console.log(oItem);
	_gBtrObj.btr_guid = oItem.btr_guid;
	_gBtrObj.approved_date = oItem.approvate_date;
	_gBtrObj.approval_status= oItem.approval_status;
	_gBtrObj.requestor_uni = oItem.requestor_uni_code;
	_gBtrObj.requestor_uni_key = oItem.requestor_uni_key;
	_gBtrObj.budget_type= oItem.budget_type;
	_gBtrObj.title = oItem.title;
	_gBtrObj.approval_status = oItem.approval_status;
	_gBtrObj.approved_date = oItem.approved_date;
	_gBtrObj.Attachments = 1;
	_gBtrObj.budget_type= oItem.budget_type;
	_gBtrObj.created= oItem.created;
	_gBtrObj.btr_key = oItem.btr_key;
	_gBtrObj.internal_state = oItem.internal_state;
	_gBtrObj.life_cycle = oItem.life_cycle;
	_gBtrObj.modified = oItem.modified;
    _gBtrObj.request_date = oItem.request_date;
    _gBtrObj.fiscal_year = oItem.fiscal_year;
	vexplanation = '';
	if (oItem.explanation != null)
	{
		vexplanation = oItem.explanation;
    }
    _gBtrObj.explanation = vexplanation;
	_gBtrObj.transfer_type = oItem.transfer_type.toLowerCase();
    _gForm_transfer_type = oItem.transfer_type.toLowerCase();
}
//Populate BTR form elements
function SetBTRForm(objBtr) {
	$('#txtBTR_Guid').val(objBtr.btr_guid);
	$('#txtBTR_ApprovedDate').val(objBtr.approved_date);
	$('#txtBTR_ApprovalStatus').val(objBtr.approval_status);
	$("#lblRequester").html(objBtr.requestor_uni);
	$("#txtBTR_Requestor").val(objBtr.requestor_uni);
	//$('#budgetType').val(objBtr.budget_type);
    $('#budgetType_' + objBtr.budget_type).attr('checked', true);

	$('#txtBTR_title').val(objBtr.title);
	$("#divStatus"+ objBtr.life_cycle).css('background-color','red').css('color','white');
	$('#txtexplanation').val(objBtr.explanation);	
		
	$('#txtBTR_life_cycle').val(objBtr.life_cycle); //Put the request into the default starting state Draft
    $('#txtBTR_transfer_type').val(objBtr.transfer_type); //Put the request into the default starting state Draft
    SetFiscalYears(objBtr.fiscal_year);
    $('#fiscalYear').val(objBtr.fiscal_year);
}
	
function GetAccountAndBalanceFromCache(acctData, acctField,balanceField,indexKey,acctKey) {
    var _select = new Array();
    
	_select.push('<option value="-1">Select Account</option>');

	for(var index =0; index < acctData.length; index++) {
		var obj = acctData[index];
		if (obj.index_key == indexKey)
		{
            _select.push('<option value="'+obj.account_key+'">'+obj.account_number_description+'</option>');
            if ((acctKey == obj.account_key) && (balanceField !== null))
            {
                $(balanceField).text(Utils.Currency.format(obj.account_balance));
            }
        }
	}
	$(acctField).empty().append(_select.join());			
	$(acctField).val(acctKey);
}

function SetFiscalYears(startingYear)
{

    function getFiscalYear(currentYear, currentMonth) {
        return currentMonth > 8
            ? currentYear + 1
            : currentYear;
    }

    var dateObj = new Date();
    var currentYear = getFiscalYear(dateObj.getFullYear(), dateObj.getMonth() + 1);

    if (startingYear == null)
    {
        var years = [currentYear, currentYear + 1];
    }
    else
    {
        var deltaYear = currentYear - startingYear; //logic for later when Omar provides the calculation
        var years = [currentYear, currentYear + 1];
    }

    var _select = [];
    for (var counter = 0; counter < years.length; counter++)
    {
        var year = years[counter];
        if (counter === 0) {
            _select.push('<option selected value="' + year + '">' + year + '</option>')
        } else {
            _select.push('<option value="' + year + '">' + year + '</option>');
        }
    }

    $('#fiscalYear').html(_select.join(''));	
}

function LoadFiles() {
    FileUtils.GetFilesFromFolder(_gBtrObj.btr_guid,
        function (fileList) {
            if ((fileList.files == null) || (fileList.files.length == 0)) {
                return;
            }

            $('#divSavedFiles').css('display', 'block'); //we have files display them
            var ulString = Array();
            ulString.push("<ul style='list-style-type: none'>");
            for (var index = 0; index < fileList.files.length; index++) {
                ulString.push("<li>" + fileList.files[index].name + "</li>");
            }
            ulString.push("</ul>");
            $("#divSavedfileList").html(ulString.join(''));
        }
    );

}

function DisplayPendingFiles(elementId)
{
    $('#divPendingFiles').css('display', 'block'); 
    var files = document.getElementById(elementId).files;
    var ulString = Array();
    ulString.push("<ul style='list-style-type: none'>");
    for (var index = 0; index < files.length; index++) {
        ulString.push("<li>" + files[index].name + "</li>");
    }
    ulString.push("</ul>");
    $("#divPendingfilesList").html(ulString.join(''));
}

function BuildForm(cbFunc,cbFuncFailed) {
	if (_gForm_Mode == _gConst_Form_ModeNew)
	{
		//Treat as new form
        _gBtrObj.Requester = BTR.User.Current.uni_code;
        $("#lblRequester").html(BTR.User.Current.uni_code); 
        $("#txtBTR_Requestor").val(BTR.User.Current.uni_code);
		$('#txtBTR_life_cycle').val('Draft'); //Put the request into the default starting state Draft
		$('#txtBTR_transfer_type').val(_gForm_transfer_type); //Put the request into the default starting state Draft
		$('#divStatusNew').css('background-color','red').css('color','white');
		//Add rows to the screen
		_gRptSect_From.AddRow();
        _gRptSect_To.AddRow();


        SetFiscalYears(null);
		//Set the Item Index to a default value
		$('#fromTAItemIndex'+_gRptSect_From.GetRowIds()[0]).val(1);
		$('#toTAItemIndex'+_gRptSect_To.GetRowIds()[0]).val(1);
		cbFunc(); //Need to fix because of async
	}
	else
	{
		//get the current BTR record
			
		BTR.BudgetTransfer.Item(_gForm_ID, 
			function(dataBtrObj) {
						
				PopulateBTRObject(dataBtrObj);
				SetBTRForm(_gBtrObj);
				var aryAccountIndexKeys= new Array();
						
				BTR.TransferActivity.GetBTRAssociatedTransferActivities(_gForm_ID, 
					function (dataArray) {
						//build an array of account keys
						for(var index=0; index < dataArray.length; index++) {
							aryAccountIndexKeys.push(dataArray[index].index_key);
						}
						//submit all of the account keys at one time
						if (aryAccountIndexKeys.length < 2)
						{
							throw "Error loading the indices and accounts";
						}
						GetAllAccountsAndBalances(aryAccountIndexKeys, 
							function(acctData) {
								PopulateTransferActivities(dataArray,acctData);
                                $('#totalFromValue').text(Utils.Currency.format(Formula_CalcTotals('[rel="from.select.amounttotransfer"]')));
                                $('#totalToValue').text(Utils.Currency.format(Formula_CalcTotals('[rel="to.select.amounttorecieve"]')));
								cbFunc();
							}
						);
					}
				); // end of Get All Accounts and Balances
                LoadFiles();

			}, 
			function() {
				throw 'Failed to get Budget Transfer Record';
			}
		); //end of BudgetTransfer Item function

	}	
}
	
function BuildForm_Populate_IndicesToSide(callback) {
	//Get the full index list
	PopulateElement(GetIndicesAll,function(data2) {
		var _select = new Array();
		$.each(data2, function(index,obj) {
            _select.push('<option value="'+obj.index_key+'">'+obj.index_number_description+'</option>');
		});
		
		$('#toTAIndex_FXXXXX').append(_select.join());	
		callback();
	});
}

function BuildForm_Populate_IndicesFromSide(callback) {
	//Get the denormalized index list
    PopulateElement(GetIndicesAssigndByUni,function(data) {
			var _select = new Array();

			$.each(data, function(index,obj) {
				_select.push('<option value="'+obj.index_key+'">'+obj.index_number_description+'</option>');
			});
				
			$('#fromTAIndex_FXXXXX').append(_select.join());
			callback();
		});
}	
var waitDialog  = null;
function HideOverlay(){
	try {
        waitDialog.close();
        waitDialog = null;
	} 
    catch (ex) {
        console.log(ex);
    }

}
function ShowWaitDialog(){
    try {
        if (waitDialog == null) {
            waitDialog = SP.UI.ModalDialog.showWaitScreenWithNoClose('Processing...', 'Please wait while request is in progress...', 76, 330);
        }
    } 
    catch (ex) {
        console.log(ex);
    }
}
function ShowOverlay(toggle) {
	if (toggle)
	{
		/*var over = '<div id="overlay"><img id="loading" src="../images/animated_wait.gif"></div>';
	    $(over).appendTo('body');*/
        try {
            if (waitDialog == null) {
                waitDialog = SP.UI.ModalDialog.showWaitScreenWithNoClose('Processing...', 'Please wait while request is in progress...', 76, 330);
            }
        } 
        catch (ex) {
            console.log(ex);
        }
	}
	else
	{
		/*$("#overlay").css("display","none");*/
		try {
            waitDialog.close();
            waitDialog = null;
		} 
        catch (ex) {
            console.log(ex);
        }
	}
}	


function LoadForm() {

	BuildForm_Populate_IndicesFromSide(function () {
		BuildForm_Populate_IndicesToSide(function () {
			BuildForm(
				function () {
					waitingDialog.hide();
				}, 
				function() {
					waitingDialog.hide();
				}
			);
		});
	});
}
	
function LoadDataGrid() {
	//initialize the first rows of the repreating section
	_gRptSect_From = DataGrid($('#fromTA_FXXXXX'),'fromColumn');
	_gRptSect_To = DataGrid($('#toTA_FXXXXX'),'toColumn');
			
	//Register Events
	//wire up for After a row has been added
	_gRptSect_From.OnAddRow(function() {
		console.log('fired - onAddRow ');
		//fire onchange event get Accounts when an Index is changed
		$('[rel="from.select.index"]').on('change',function(e) {
			SetFormStatus('dirty');
			console.log('fired to selected index' + e.currentTarget.value);
				
			var controlId = '#'+e.currentTarget.id.replace(/Index/,'Account')
			GetAccounts(controlId,e.currentTarget.value);
		});

		//when an account is selected change the balance
		$('[rel="from.select.account"]').on('change',function(e) {
			SetFormStatus('dirty');
			console.log('fired to selected index' + e.currentTarget.value); 	//fromTABalance_FXXXXX
				
			var id = e.currentTarget.id.split("_");
			var controlId = '#fromTABalance_FXXXXX'.replace(/FXXXXX/,id[1]);
			GetAccountBalance(controlId,e.currentTarget.value);
		});

		//only allow numeric and arrow keys
		$('[rel="from.select.amounttotransfer"]').on('keydown',function(e){
			Utils.KeyEvent_NumericOnly(e);
		});

		//when the amount to transfer is changed update the rolling total
        $('[rel="from.select.amounttotransfer"]').on('change', function (e) {
            //check to see if there's enough funds to transfer
            var controlId = '#fromTABalance_FXXXXX'.replace(/FXXXXX/, e.target.id.split('_')[1]);

            var currentBalance = Number($(controlId).text().replace(/[^-|^0-9\.]+/g, ""));
            if (isNaN(currentBalance))
            {
                //TODO: log error
                //return;
            }
            else {
                var amounttotransfer = Number(e.target.value);
                if ((currentBalance < 0) || ((currentBalance - amounttotransfer) < 0))
                {
                    e.target.value = '';
                    //TODO: This should come from a SharePoint list
                    alert('Error - The account that you are trying to select from is either negative or does not have significent funds.');
                }
            }

			SetFormStatus('dirty');
            $('#totalFromValue').text(Utils.Currency.format(Formula_CalcTotals('[rel="from.select.amounttotransfer"]')));
		});
	});
		
		
	//Event wire up for the To Side
	_gRptSect_To.OnAddRow(function () {
		$('[rel="to.select.index"]').on('change',function(e) {
			SetFormStatus('dirty');
			console.log('fired to selected index' + e.currentTarget.value);
			var controlId = '#'+ e.currentTarget.id.replace(/Index/,'Account')
			GetAccounts(controlId,e.currentTarget.value);
		});
		//only allow numeric and arrow keys
		$('[rel="to.select.amounttorecieve"]').on('keydown',function(e){ 
			Utils.KeyEvent_NumericOnly(e);
		});

        $('[rel="to.select.account"]').on('change', function (e) {
            SetFormStatus('dirty');
            console.log('fired to selected index' + e.currentTarget.value); 	//toTABalance_FXXXXX

            var id = e.currentTarget.id.split("_");
            var controlId = '#toTABalance_FXXXXX'.replace(/FXXXXX/, id[1]);
            GetAccountBalance(controlId, e.currentTarget.value);
        });

		$('[rel="to.select.amounttorecieve"]').on('change',function(e) {
			SetFormStatus('dirty');
            $('#totalToValue').text(Utils.Currency.format(Formula_CalcTotals('[rel="to.select.amounttorecieve"]')));
		});
	});
	
}
	
function LoadUserInfromation(callback) {
        BTR.User.GetCurrent(_gUser_LoginName, 
		function() {
            $("#userField").html(BTR.User.UserField());
			callback();
		},
        function () {
            //TODO fix with logging
            alert('User profile not found  ' + _gUser_LoginName);
            //reutrn to the main screen
		}
	);
}


function PageStart() {
    UI.Wizard.Initialize();
    UI.Wizard.AddValidation(1, function (currentPage) {
        return IsFormDataValid(currentPage);
    });
    UI.Wizard.AddValidation(2, function (currentPage) {
        return IsFormDataValid(currentPage);
    });

    UI.Wizard.AddValidation(3, function (currentPage) {
        return IsFormDataValid(currentPage);
    });

    BTR.BudgetTransfer.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);
    BTR.TransferActivity.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);

    BTR.User.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);
    BTR.Index.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);
    BTR.Account.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);
    Banner.JV.SiteUrl(GlobalConfig.Banner.Rest_Api_Url);
    LoadUserInfromation(function () {
        LoadDataGrid();
        LoadForm();
    });
}
	

//Global Variables

//Constants
var _gConst_Form_ModeNew= 1;
var _gConst_Form_ModeEdit = 2;	
var _gForm_Saved = 0;
var _gConstForm_transfer_type_IntraDept = 'intradepartmental';
var _gConstForm_transfer_type_InterDept = 'interdepartmental';
var _gConstBTRServiceUrl = GlobalConfig.BTR.Rest_Api_Url;
	
//User Attributes
var _gUser_LoginName = _spPageContextInfo.userLoginName;
var _gUser_UserDept = null; //TODO: Fix this 

//Form Variables
var _gForm_Saved_BTR = 0;
var _gForm_Saved_TransferActivities = 0;
var _gForm_ID = Utils.GetParamByName("rID");
var _gForm_transfer_type = Utils.GetParamByName("transfer_type");
if (_gForm_transfer_type == null)
{
	_gForm_transfer_type = Utils.GetParamByName("transfertype");
}
	
var _gForm_Mode = ((_gForm_ID == null) ? 1 : 2); // Form Mode 1= New, 2=Edit
var _gForm_IsDirty = false;
var _gForm_Referrer = document.referrer;

//Variables
var _gData_IndexDenorm;
var _gData_IndexTo;
var _gBtrObj = {btr_key:0, title: '', budget_type: '', approved_date:'', approval_status: '', Requester: '', Author: '', Attachments: '', Created: '', Editor: '', Guid: '', internal_state: '', life_cycle:'', transfer_type:''};
var _gRptSect_From;
var _gRptSect_To;
	
if (_gForm_transfer_type !== null)
{
	_gForm_transfer_type = _gForm_transfer_type.toLowerCase();
}


//starting point for execution of javascript code for the page
$(document).ready(function () {
    waitingDialog.show();
    try {
        BTR.Global.PageStart = function () {
            PageStart();
        };
        BTR.Global.Initialize(); //initialize global variables
    }
    catch (err) {
        waitingDialog.hide();
        Logger.logit("ERROR", "BTREdit.aspx", 1, "Page_Document_ready", null);
    }
});
