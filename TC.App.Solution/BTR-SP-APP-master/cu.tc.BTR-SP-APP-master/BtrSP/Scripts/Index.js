﻿var BTR = BTR || {};
BTR.Index = {};
BTR.Index = Object.create(BTR.ApiConnector);  //inherit from the ApiConnector object

BTR.Index.WebApiUrlIndices  = "Indices";
BTR.Index.WebApiUrlIndicesOwnedByDept  = "IndicesByOwner";
BTR.Index.WebApiUrlIndicesByDept = "IndicesByDept";
BTR.Index.WebApiUrlIndicesAssignedByUni = "IndicesAssignedByUni";
BTR.Index.ListBaseApiUrl = "/api/Indices/";


//--------------------------------------------------------- Read  Item  ---------------------------------------------------------
//@Function GetIndicesOwnedByDept 
//@Description: Get all the indices 
	BTR.Index.GetIndices = function(scb,fcb) {

        var dataError = this.ErrorObject();
        if ((uni == null) || (uni.length == 0)) {
            dataError.ErrorCode = "INDEX_JS_P01";
            dataError.StatusText = "Parameter UNI can not be blank"; //this is set as a default, if there's an error code associated it will override this value
            dataError.Message = dataError.StatusText;
            fcb(dataError);
        }


        var filter = 'uni=' + uni;
        if (dept_code != null) {
            //filter += '&dept_code='+dept_code;
            filter += '&dept_key=' + dept_code;
        }


        try {
            this.ExecuteApiGet(BTR.Index.WebApiUrlIndices, null, scb, fcb);
        }
        catch (err) {
            console.error(arguments.callee.name + ':' + err);
            var errObj = this.CreateErrMsg('INDEX_JS_API_INDICES', err, arguments.callee.name + ' - ' + err);
            fcb(errObj);
        }	
	}


//@Function GetIndicesOwnedByDept 
//@Description: Get all the indices owned by the user and filtered by department, if dept_code is not supplied then results will not be filtered by department
//@Parameters: uni = Required, dept_code = optional
	BTR.Index.GetIndicesOwnedByDept = function(uni,dept_code,scb,fcb) {
        var dataError = this.ErrorObject();
		
		if ((uni == null) || (uni.length == 0)) {
            dataError.ErrorCode = "INDEX_JS_P02";
		  	dataError.StatusText = "Department key or Uni ID can not be blank";
		  	dataError.Message = dataError.StatusText;
			fcb(dataError);
		}
		
		var filter = 'uni='+uni;
		if (dept_code != null)
		{
			//filter += '&dept_code='+dept_code;
            filter += '&dept_key=' + dept_code;
        }
        try {
            this.ExecuteApiGet(BTR.Index.WebApiUrlIndicesOwnedByDept, filter, scb, fcb);
        } catch (err) {
            console.error(arguments.callee.name + ':' + err);
            var errObj = this.CreateErrMsg('INDEX_JS_API_INDICESOWNEDBYDEPT', err, arguments.callee.name + ' - ' + err);
            fcb(errObj);

        }
	}

    //@Function GetIndicesAssignedByUni
    //@Description: Get all the indices owned by the user and filtered by department, if dept_code is not supplied then results will not be filtered by department
    //@Parameters: uni = Required, dept_code = optional
    BTR.Index.GetIndicesAssignedByUni = function (uni, scb, fcb) {
        var dataError = this.ErrorObject();

        if ((uni == null) || (uni.length == 0)) {
            dataError.ErrorCode = "INDEX_JS_P03";
            dataError.StatusText = "Uni ID can not be blank";
            dataError.Message = dataError.StatusText;
            fcb(dataError);
        }
        try {
            var filter = 'uni=' + uni;
            this.ExecuteApiGet(BTR.Index.WebApiUrlIndicesAssignedByUni, filter, scb, fcb);

        } catch (err) {
            console.error(arguments.callee.name + ':' + err);
            var errObj = this.CreateErrMsg('INDEX_JS_API_ASSIGNEDBYUNI', err, arguments.callee.name + ' - ' + err);
            fcb(errObj);
        }
    }

//@Function GetIndicesOwnedByDept 
//@Description: Get all the indices filtered by department
	BTR.Index.GetIndicesByDept = function(dept_code,scb,fcb) {
        //var dataError = this.ErrorObject();
        //it is optional for dept_code to be null so we don't need to check the value
		
		var filter = '';
		if (dept_code !== null) {
			//filter = "dept_code="+dept_code;
            filter = "dept_key=" + dept_code;
        }
        try {
            this.ExecuteApiGet(BTR.Index.WebApiUrlIndicesByDept, filter, scb, fcb);
        } catch (e) {
            console.error(arguments.callee.name + ':' + err);
            var errObj = this.CreateErrMsg('INDEX_JS_API_BYDEPT', err, arguments.callee.name + ' - ' + err);
            fcb(errObj);
        }
	}