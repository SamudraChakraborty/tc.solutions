﻿var Banner = Banner || {};
Banner.JV = {};
Banner.JV = Object.create(BTR.ApiConnector);  //inherit from the ApiConnector object

Banner.JV.Component = {}
Banner.JV.Component.Name = 'JV';

Banner.JV.ListBaseApiUrl = "/api/jv/";
	
//--------------------------------------------------------- Read  Item  ---------------------------------------------------------

//@Function Create 
//@Description: Create a new JV 
//@Parameters: btr_key = Required, successful callback, failed callback
Banner.JV.Create = function (btr_key, scb, fcb) {
    var dataError = this.ErrorObject();
    if ((btr_key == null) || (btr_key.length == 0)) {
        dataError.ErrorCode = "BANNER_JV_JS_P01";
        dataError.StatusText = "Parameter btr_key can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        var filter = 'btr_key=' + btr_key;
        this.ExecuteApiPost('create', filter, scb, fcb);
    }
    catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('BANNER_JV_JS_API_ITEM', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};