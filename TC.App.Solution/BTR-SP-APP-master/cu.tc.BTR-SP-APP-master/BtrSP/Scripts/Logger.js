var Logger = {
    // This module performs logging to a SP List and also handles error byt looking up an error on a SP list

    appLogListName: 'AppLog',   // the list where the log will be written to
    errorCodeListName: 'ErrorCode',  // the list where error codes will be read from

    // Logit: Log to list, look up error and display to user
    logit: function (state, page, errorcode, moreinfo, payload) {
        debugger;
        var message = "";
        if (state === "ERROR") {  // an error was detected, log and alert
            // lookup title, description & ActionLink from from the ErrorCode List
            var title = "", description = "", actionLink = "";
            Logger.getErrorDetailsById(errorcode, function (data) {  // read the error code title, details and action link
                if (data.IsError) {
                    description = data.ErrorMsg;  // set the error message
                }
                else {
                    title = data.Title;
                    description = data.ErrorDescription;
                    actionLink = data.ActionLink;
                }
                var arrData = {};
                arrData.Title = state;
                arrData.ErrorCode = errorcode;
                arrData.Payload = JSON.stringify(payload);
                arrData.MoreInfo = JSON.stringify(moreinfo);
                arrData.CurrentUserContext = JSON.stringify(_spPageContextInfo);
                Logger.createNewItem(Logger.appLogListName, arrData);
                message = "<b>ERROR</b><br>" + "<b>Title</b> : " + title + "<br>" + "<b>ErrorCode</b> : #" + errorcode + "<br>" + "<b>Description</b><br>" + description + "<br>";
                Logger.displayAppMessage("Error", message);  // display error to user
                message = "BTR ERROR :" + errorcode + " : " + page + " :" + moreinfo + " : " + payload;
                console.log(message);  // log to the console also
            });
        } else {  // no error, just log
            var arrData = {};
            arrData.Title = state;
            arrData.ErrorCode = errorcode;
            arrData.Payload = JSON.stringify(payload);
            arrData.MoreInfo = JSON.stringify(moreinfo);
            arrData.CurrentUserContext = JSON.stringify(_spPageContextInfo);

            Logger.createNewItem(Logger.appLogListName, arrData);
            message = "HR Portal LOG : " + page + " : " + moreinfo + " : " + payload;
            console.log(message);
        }
    },
    // format a nice message for the user
    displayAppMessage: function (title, msg, callback) {
        $('#AppLoggerModal').on('hidden.bs.modal', function () {
            if (typeof (callback) === "function") {
                callback();
            }
        });
        if (($("#AppLoggerModal").data('bs.modal') || {}).isShown) {
            $("#loggerMsgBody").append("<hr><br>" + msg);
        }
        else {
            $("#loggerMsgTitle").html(title);
            $("#loggerMsgBody").html(msg);
            $("#AppLoggerModal").modal();
        }

    },
    // write a item to the error log list
    createNewItem: function (listName, arrayFieldValues, callback) {
        $pnp.sp.web.lists.getByTitle(listName).items.add(arrayFieldValues).then(function (result) {
            if (typeof (callback) === "function") {
                callback(result);
            }
            else if (typeof (callback) === "string") {
                alert(callback);
            }
            else {
                console.log("List Item Created Successfully");
            }
        });
    },
    // look up an error message by ID on the error code list
    getErrorDetailsById: function (errorCode, callback, isError) {
        var errorCodeList = $pnp.sp.web.lists.getByTitle(Logger.errorCodeListName);
        errorCodeList.items.getById(errorCode).get().then(function (data) {
            callback(data);
        }).catch(function (err) {
            if (!isError) {
                Logger.getErrorDetailsById(1, callback, true);
            }
            else {
                var data = {};
                data.IsError = true;
                data.ErrorMsg = "No Details Found for ErrorCode";
                callback(data);
            }
        });
    }
};