﻿var BTR = BTR || {};
BTR.BudgetTransfer = {};
BTR.BudgetTransfer = Object.create(BTR.ApiConnector);  //inherit from the ApiConnector object

BTR.Component = {}
BTR.Component.Name = 'BudgetTransferRequest';

BTR.BudgetTransfer.ListBaseApiUrl = "/api/BTR/";

//reinserted because a lot of code depends on this
BTR.BudgetTransfer.DataItem = {
    Create: function () {
        var jsonData = {};
        return jsonData;
    }
};

//@Function Item
//@Description: Function used to to return a BTR item by ID

BTR.BudgetTransfer.Item = function (listItemId, scb, fcb) {

    var dataError = this.ErrorObject();
    if ((listItemId == null) || (listItemId.length == 0)) {
        dataError.ErrorCode = "BTR_JS_P01";
        dataError.StatusText = "Parameter listItemId can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        var filter = 'btr_key=' + listItemId;
        this.ExecuteApiGet('item', filter, scb, fcb);
	}
	catch (err) {
        console.error(arguments.callee.name+':'+err);
        var errObj = this.CreateErrMsg('BTR_JS_API_ITEM', err, arguments.callee.name+' - ' + err);
	    fcb(errObj);
	}	
};

//@Function Item
//@Description: Function used to to return a BTR item by ID

BTR.BudgetTransfer.ItemTableRecord = function (userId, scb, fcb) {

    var dataError = this.ErrorObject();
    if ((userId == null) || (userId.length == 0)) {
        dataError.ErrorCode = "BTR_JS_P02";
        dataError.StatusText = "Parameter UserID can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        var filter = 'requestor_uni_key=' + userId;
        this.ExecuteApiGet('ItemsByUni', filter, scb, fcb);
    }
    catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('BTR_JS_API_ITEMTABLE', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }	
};



//@Function ResponsiblePerson
//@Description: Function used to to return transfer activity for responsible person

BTR.BudgetTransfer.ResponsiblePerson = function (btr_key, scb, fcb) {

    if ((btr_key == null) || (btr_key.length == 0)) {
        dataError.ErrorCode = "BTR_JS_P05";
        dataError.StatusText = "Parameter btr_key can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    var filter = "btr_key=" + btr_key;

    try {
        this.ExecuteApiGet('ResponsiblePersons', filter, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('BTR_JS_API_ITEM', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};

//@Function ResponsiblePerson
//@Description: Function used to to return transfer activity for responsible person

BTR.BudgetTransfer.BudgetAdministrator = function (btr_key, scb, fcb) {

    if ((btr_key == null) || (btr_key.length == 0)) {
        dataError.ErrorCode = "BTR_JS_P05";
        dataError.StatusText = "Parameter btr_key can not be blank"; //this is set as a default, if there's an error code associated it will override this value
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    var filter = "btr_key=" + btr_key;

    try {
        this.ExecuteApiGet('BudgetAdministrators', filter, scb, fcb);
    } catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('BTR_JS_API_ITEM', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }
};



//@Function Item
//@Description: Function used to to return a BTR item by ID

BTR.BudgetTransfer.Create = function (jsonData, scb, fcb) {
    var dataError = this.ErrorObject();
    if (jsonData == null)  {
        dataError.ErrorCode = "BTR_JS_P03";
        dataError.StatusText = "No data has been specied to create budget transfer";
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        this.ExecuteApiPost('create', jsonData,null, scb, fcb);
    }
    catch (err) {

        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('BTR_JS_API_CREATE', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }	
};

//@Function Update
//@Description: Function used to to return a BTR item by ID
BTR.BudgetTransfer.Update = function (listItemId, jsonData, eTag, scb, fcb) {
    //debugger;

    var dataError = this.ErrorObject();
    if (jsonData == null) {
        dataError.ErrorCode = "BTR_JS_P04";
        dataError.StatusText = "No data has been specied to update budget transfer";
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        this.ExecuteApiPut('update', jsonData, null, scb, fcb);
    }
    catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('BTR_JS_API_UPDATE', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }	
}; // end of function


//@Function Update
//@Description: Function used to to return a BTR item by ID
BTR.BudgetTransfer.Update = function (listItemId, jsonData, eTag, scb, fcb) {

    var dataError = this.ErrorObject();
    if (jsonData == null) {
        dataError.ErrorCode = "BTR_JS_P04";
        dataError.StatusText = "No data has been specied to update budget transfer";
        dataError.Message = dataError.StatusText;
        fcb(dataError);
    }

    try {
        this.ExecuteApiPut('update', jsonData, null, scb, fcb);
    }
    catch (err) {
        console.error(arguments.callee.name + ':' + err);
        var errObj = this.CreateErrMsg('BTR_JS_API_UPDATE', err, arguments.callee.name + ' - ' + err);
        fcb(errObj);
    }	
}; // end of function