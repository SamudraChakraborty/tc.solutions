﻿var BTR = BTR || {};
BTR.Global = {};

BTR.Global.PageStart = null;

BTR.Global.Initialize = function () {

    $pnp.sp.web.lists.getByTitle("Configurations").items.filter("active eq 1").get().then(r => {
        //debugger;
        var key;
        var value;
        for (var index = 0; index < r.length; index++) {
            value = r[index].value.toLowerCase();
            key = r[index].Title.toLowerCase();
            switch (key) {
                case 'btr.rest_api_url':
                    GlobalConfig.BTR.Rest_Api_Url = r[index].value;
                    break;
                case 'banner.rest_api_url':
                    GlobalConfig.Banner.Rest_Api_Url = r[index].value;
                    break;
            }
        }
        if (BTR.Global.PageStart != null) {
            this.PageStart();
        }
    }).catch(function (err) {
        console.error(err);
        throw err;
    });
}; 