﻿var BTR = BTR || {};
    BTR.User = {};

	BTR.User.ListApiUrl = "/_api/web/lists/getbytitle('users')";
    BTR.User.ListBaseApiUrl = "/api/user/";

	BTR.User.SiteUrl = function(siteUrl) {
		this._siteUrl = siteUrl;
	};

    BTR.User.AppWebUrl = function (appWebUrl) {
        this._appWebUrl = appWebUrl;
    }

    BTR.User.HostWebUrl = function (hostWebUrl) {
        this._hostWebUrl = hostWebUrl;
    }

	BTR.User.user_uni = '';
	BTR.User.user_uni_key = 0;
	BTR.User.user_lastName = '';
	BTR.User.user_firstName = '';
	BTR.User.user_email = '';
	BTR.User.user_dept_key = '';
	

    BTR.User.GetFakeUser = function (upn, scb)
    {
        var users = new Array();
        users.push({'upn': 'kcooper@teacherscollegecolumbia.onmicrosoft.com', 'Uni': 'kcooper', 'last_name': '', 'first_name': '', 'full_name': '', 'email': '', 'dept_key': 1, 'uni_key': 36 });
        users.push({'upn':'ken.cooper@higherwiretech.com', 'Uni': 'kcooper', 'last_name': '', 'first_name': '', 'full_name': '', 'email': '', 'dept_key': 1, 'uni_key': 36 });
        users.push({'upn': 'jeff.barnes@higherwiretech.com', 'Uni': 'jbarnes', 'last_name': '', 'first_name': '', 'full_name': '', 'email': '', 'dept_key': 1, 'uni_key': 9 });
        users.push({'upn': 'jbarnes@teacherscollegecolumbia.onmicrosoft.com', 'Uni': 'jbarnes', 'last_name': '', 'first_name': '', 'full_name': '', 'email': '', 'dept_key': 1, 'uni_key': 9 });
        users.push({'upn':'oam2103@tc.columbia.edu', 'Uni': 'oam2103', 'last_name': '', 'first_name': '', 'full_name': '', 'email': '', 'dept_key': 1, 'uni_key': 3 });
        users.push({ 'upn': 'omar.mayyasi@tc.columbia.edu', 'Uni': 'oam2103', 'last_name': '', 'first_name': '', 'full_name': '', 'email': '', 'dept_key': 1, 'uni_key': 3 });
        users.push({ 'upn': 'mayyasi@tc.columbia.edu', 'Uni': 'oam2103', 'last_name': 'Mayyasi', 'first_name': 'Omar', 'full_name': '', 'email': 'omar.mayyasi@tc.columbia.edu', 'dept_key': 1, 'uni_key': 3 });
        users.push({ 'upn': 'shihmei.kong@higherwiretech.com', 'Uni': 'shihmei', 'last_name': '', 'first_name': '', 'full_name': '', 'email': '', 'dept_key': 1, 'uni_key': 36 });
/*
///End users:
Kristin Gorski < kg2366@tc.columbia.edu>
Aimee Katembo < alf22@tc.columbia.edu>
Maria Lamadrid < mel31@tc.columbia.edu>
Sherene Alexander < tsa2002@tc.columbia.edu>
Dianne Marcucci- Sadnytzky < dm136@tc.columbia.edu>
Thomas Rock < tpr4@tc.columbia.edu>
Robert Graham IV < rwg2106@tc.columbia.edu>  

//Budget Office staff:
Joan Anderson < jda73@tc.columbia.edu>
Herminio Quinones < hq2144@tc.columbia.edu>
Jason Phoel < jp3402@tc.columbia.edu>
Bryan Senker < bs3043@tc.columbia.edu>
Ivy Fernandez < if2235@tc.columbia.edu>

//Grants Office staff:
John Hernandez jlh2172@tc.columbia.edu
David Novick dn36@tc.columbia.edu
Matthew Turk turk@tc.edu (don’t know what his uni is)
Maria Gervacio mrg2112@tc.columbia.edu
Kim Santoro kms2144@tc.columbia.edu
        
*/
        upn = upn.toLowerCase();
        for (var index = 0; index < users.length; index++)
        {
            if (users[index].upn.toLowerCase() == upn)
            {
                scb(users[index]);
            }
        }
    }


    BTR.User.InfoRecord = function (userUPN, scb, fcb) {
        //TODO: this is bad but having trouble with SharePoint list
        this.GetFakeUser(userUPN, scb);
        return;

        if ((typeof this._siteUrl) == 'undefined') {
			fcb();
			return;
		}

        //Read the timestamp for concurrent situations
        var webApiUrl = 'https://btrservices.azurewebsites.net' + this.ListBaseApiUrl + 'byUPN?'+userUPN;
        $.ajax({
            url: webApiUrl,
				method: "GET",
				headers: { "Accept": "application/json; odata=verbose" },
                success: function (data) {
                    //debugger;
                    if (data.d.results.length !== 1)
					{
						fcb();
					}
					scb(data.d.results[0]);
				  },
                error: function (data) {
                    debugger;
					fcb(data);
				 }
		});
	};

    BTR.User.InfoRecordWebApi = function (userUPN, scb, fcb) {
        //TODO: this is bad but having trouble with SharePoint list
        //debugger;

        if ((typeof this._siteUrl) == 'undefined') {
            fcb();
            return;
        }

        //Read the timestamp for concurrent situations
        
        $.ajax({
            url: this._siteUrl + BTR.User.ListBaseApiUrl + selectStmt + filter,
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                if (data.d.results.length != 1) {
                    fcb();
                }
                scb(data.d.results[0]);
            },
            error: function (data) {
                fcb(data);
            }
        });
    };

    BTR.User.Initialize = function (userUPN, scb, fcb) {
		BTR.User.InfoRecord(userUPN,
			function(userData) {
				if(userData){
					BTR.User.uni_code = userData.Uni;
					BTR.User.last_name = userData.LastName;
					BTR.User.first_name = userData.FirstName;
					BTR.User.full_name = userData.FullName;
					BTR.User.email = userData.Email;
					BTR.User.dept_key = userData.dept_key;
					BTR.User.uni_key = userData.uni_key;
				}
				scb();
			}
			,fcb
		);
	};