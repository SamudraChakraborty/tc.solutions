﻿var FileUtils = FileUtils || {};

FileUtils.GetDocumentLibraries = function GetDocumentLibraries() {
    $pnp.sp.site.getDocumentLibraries(_spPageContextInfo.webAbsoluteUrl).then(function (data) {
        var strTitle = "";
        for (var i = 0; i < data.length; i++) {
            strTitle += data[i].Title + '\r\n';
        }
        document.getElementById("messages").innerText = strTitle;

    }).catch(function (err) {
        console.error(arguments.callee.name + ':' + err);
    });

}

FileUtils.CreateFolder = function CreateFolder(folderName)
{
    $pnp.sp.web.lists.getByTitle(GlobalConfig.BTR.BTR_DocumentLibrary)
        .rootFolder
        .folders
        .add(folderName).then(function (data) {
            console.log(data);
        }).catch(function (err) {
            console.error(arguments.callee.name + ':' + err);
        });
}

FileUtils.GetFilesFromFolder = function GetFilesFromFolder(folderName,callBack)
{
    var url = _spPageContextInfo.webServerRelativeUrl + GlobalConfig.BTR.BTR_DocumentLibrary_RelPath + folderName + "/";
    var filelist = { files: [] };
    
    $pnp.sp.web
        .getFolderByServerRelativeUrl(url) // Here comes a folder/subfolder path
        .files
        .expand("Files/ListItemAllFields")
        .select("Title","Name", "FileRef", "FieldValuesAsText/MetaInfo")
        .get().then(function (items) {
            for (var i = 0; i < items.length; i++) {
                console.log(items[i]);
                filelist.files.push({ 'name': items[i].Name, 'title': items[i].Title});
            }
            callBack(filelist);
        });
}

FileUtils.FolderExistsTest = function FolderExists(urlPath)
{
    $pnp.sp.web.lists.getByTitle(GlobalConfig.BTR.BTR_DocumentLibrary)
        .rootFolder
        .folders.filter("Title eq 'testing'").get().then(function (result) {
            var x = result;
        });
}

FileUtils.UploadFiles = function UploadFiles(fileElementId, folderName) {
    //Get the file from File 
    var files = document.getElementById(fileElementId).files;
    var file;
    var url = _spPageContextInfo.webServerRelativeUrl + GlobalConfig.BTR.BTR_DocumentLibrary_RelPath + folderName + "/";

    ///sites/Developers/BT/BtrSP/Lists/BTRDocs/
    this.CreateFolder(folderName);
    for (var index = 0; index < files.length; index++)
    {
        file = files[index];
        console.log(file.name);
        $pnp.sp.web.getFolderByServerRelativeUrl(url).files.add(file.name, file, true).then( f=> {

            f.file.getItem().then(item => {
                item.update({
                    Title: file.name
                });
            });
            
            console.log(f.file.name + " upload successfully!");
            //document.getElementById("messages").innerHTML = file.name + " uploaded successfully!"
        }).catch(function (err) {
            console.error(arguments.callee.name + ':' + err);
       });
    }
}