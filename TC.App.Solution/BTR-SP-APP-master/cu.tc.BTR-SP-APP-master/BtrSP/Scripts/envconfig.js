﻿var GlobalConfig = GlobalConfig || {};
GlobalConfig.Banner = {};
GlobalConfig.Banner.Rest_Api_Url = null;
GlobalConfig.BTR= {};
GlobalConfig.BTR.Rest_Api_Url = null;
GlobalConfig.BTR.BTR_DocumentLibrary = 'BTRDocs';
GlobalConfig.BTR.BTR_DocumentLibrary_RelPath = '/lists/' + GlobalConfig.BTR.BTR_DocumentLibrary + '/';
GlobalConfig.BTR.UserAPI = true;

GlobalConfig.BTR.ErrorCodes = {
    'url_not_initialize': 'URL01',
    'url_not_init_btr': 'UBTR01',
    'url_not_init_ta': 'UTA01',
    'user_not_defined' : 'User not defined'
};