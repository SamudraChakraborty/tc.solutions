﻿<%@ Page language="C#" MasterPageFile="~site/_catalogs/masterpage/BTRApps.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:content contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
<!-- ================== BEGIN BASE CSS STYLE ================== -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css" />
<link rel="stylesheet" href="../Content/page_btredit.css" type="text/css"/> 
<link rel="stylesheet" href="../Content/hide_office_bar.css" type="text/css"/>
<link rel="stylesheet" href="../Content/main.css" type="text/css"/>
<style>
    #menuItem_transfer_request {
        display:none;
    }
</style>
<!-- ================== BEGIN BASE JS ================== -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.0.1/cleave.min.js"></script>
<script src="../Scripts/fetch.js" type="text/javascript"></script> 
<script src="../Scripts/es6-promise.js" type="text/javascript"></script>
<script src="../Scripts/pnp.js" type="text/javascript"></script>
<script src="../Scripts/Utils.js" type="text/javascript"></script> <!-- this has to be after SharePoint JS files -->
<script src="../Scripts/BTR.Global.js" type="text/javascript"></script> 
<script src="../Scripts/ApiConnector.js" type="text/javascript"></script> 
<script src="../Scripts/EnvConfig.js" type="text/javascript"></script>
<script src="../Scripts/FileUtils.js" type="text/javascript"></script>

<!-- ================== END BASE JS ================== -->
</asp:content>
<asp:content contentplaceholderid="PlaceHolderPageTitle" runat="server">BTR Edit</asp:content>
<asp:content contentplaceholderid="PlaceHolderMain" runat="server">
<div>
	
    <!-- Phase bar -->
	<div class="row" >
		<div class="row">
		        <!-- begin panel -->
		    <div class="col-md-1"></div>
		    <div class="col-md-10"> 
		    <div style="width:100%" class="statusDivPareent" >
		    	<div id="divStatusNew"  class="statusDivText">NEW</div>
		    	<div class="statusDiv">-</div>
		    	<div id="divStatusDraft"  class="statusDivText">DRAFT</div>
		    	<div class="statusDiv">-</div>
		    	<div id="divStatusApproval" class="statusDivText">APPROVAL</div>
		    	<div class="statusDiv">-</div>
		    	<div id="divStatusReview" class="statusDivText">REVIEW</div>
		    	<div class="statusDiv">-</div>
		    	<div id="divStatusPosted" class="statusDivText">POSTED</div>
		    	</div>
		    </div>
		    <div class="col-md-1"></div>
		</div>
		<p/>
	</div>

    <input type="hidden" id="wizardPages" value="1;2;3;4"/>
    <input type="hidden" id="wizardPagesCurrentPage" value="1"/>

    <!-- Popup-Status Begin -->
    <div class="modal fade" id="modalStatus" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="text-align:right">
		        ×</button>
                <h4 class="modal-title" id="modalStatustitle"></h4>
            </div>
            <div class="modal-body" style="text-align:center;height:200px" >
                <div style="text-align:center;position:relative;width:50%;padding-left:70px" >
	                <div style="position:relative;display:none" id="loadingMessagePassed"><img src="../images/checkmark100.png" alt=""/></div>
	                <div style="position:relative;display:none" id="loadingMessageFailed"><img src="../images/redx100-1.png" alt=""/></div>
	                <div style="position:relative;display:none" id="loadingMessageText"></div>
	                <div style="position:relative;display:none" id="loadingMessageError"></div>
	       	        <div style="position:absolute;"><div class="cssload-aim" id="loadingIcon"></div></div>
       	        </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="reloadForm();">
		        Close</button>
            </div>
            </div>
        </div>
    </div> 	
    <!-- Popup-Status End -->

    <!-- Steps header -->
    <div class="container" name="step-preview-container">
        <!-- row is for column sizing -->
        <div class="row form-group">
        <ul class="nav nav-pills nav-justified thumbnail stepPreviewPanal">
            <li class="active" onmouseover="TitleAnchorToggle(this);" onmouseout="TitleAnchorToggle(this);">
                <!-- list group groups similar items together -->
                <!-- in this case, the headings are being grouped together -->
                <a href="#1" onclick="UI.Wizard.GoToPage(1);">
                    <h4 class="list-group-item-heading">Step 1</h4>
                    <p>Complete Title and <br />Budget Type</p>
                </a>
            </li>
            <li class="disabled" onmouseover="TitleAnchorToggle(this);" onmouseout="TitleAnchorToggle(this);">
                <a href="#2" onclick="UI.Wizard.GoToPage(2);">
                    <h4 class="list-group-item-heading">Step 2</h4>
                    <p>Complete Originating Departments Information</p>
                </a>
            </li>
            <li class="disabled" onmouseover="TitleAnchorToggle(this);" onmouseout="TitleAnchorToggle(this);">
                <a onclick="UI.Wizard.GoToPage(3);">
                    <h4 class="list-group-item-heading">Step 3</h4>
                    <p>Complete Receiving Departments Information</p>
                </a>
            </li>

            <li class="disabled" onmouseover="TitleAnchorToggle(this);" onmouseout="TitleAnchorToggle(this);">
                <a onclick="UI.Wizard.GoToPage(4);">
                    <h4 class="list-group-item-heading">Step 4</h4>
                    <p>Provide explaination for transfer of funds and other relevant documents</p>
                </a>
            </li>
        </ul>
        </div>
    </div>
	
    <div class="container" name="form-container">
        <div class="form1 row setup-content">
        <div class="col-md-12 well text-center">
            <form style="padding-bottom:50px" name="frmBTR" method="get" action="">
	            <!-- Hidden Element -->
	            <div datatype="repeatingTemplate" id="fromTA_FXXXXX" style="display:none">
                    <table>
                        <tr id="fromTASection_FXXXXX">
                            <td>
               	                <img src="../images/delete_2.png" style="padding-left:5px" onclick="DeleteTransferActivity('from','_FXXXXX');" >
                                <!-- Hidden Columns -->
                                <input type="hidden" id="fromTAItemIndex_FXXXXX">
                                <input type="hidden" id="fromTAListItemId_FXXXXX">
                                <input type="hidden" id="fromTAItemAction_FXXXXX" value="create">
                                <!-- Hidden Columns -->
                            </td>
                            <td>
                            <select id="fromTAIndex_FXXXXX" rel="from.select.index"  _isrequired name="From Index">
                                <option value="" selected>Select an Index</option>
                            </select>
                            </td>
                            <td>
			                <select id="fromTAAccount_FXXXXX" rel="from.select.account"  _isrequired name="From Account">
				                <option value="" selected>Select an Account</option>
			                </select>
                            </td>
                            <td>
                            <span id="fromTABalance_FXXXXX" rel="from.select.balance">$0.00</span>
                            </td>
                            <td>
                                <input type="text" class="form-control" id="fromTAAmountToTransfer_FXXXXX" rel="from.select.amounttotransfer" placeholder="Amount" required name="Amount To Transfer"/>
                            </td>
                        </tr>
                    </table>
	            </div>
	            <div datatype="repeatingTemplate" id="toTA_FXXXXX" style="display:none">
                    <table>
                        <tr id="toTASection_FXXXXX">
                            <td>
    			                <img src="../images/delete_2.png" style="padding-left:5px" onclick="DeleteTransferActivity('TO','_FXXXXX');">
                                <!-- Hidden Columns -->
                                <input type="hidden" id="toTAItemIndex_FXXXXX"/>
                                <input type="hidden" id="toTAListItemId_FXXXXX" />						
                                <input type="hidden" id="toTAItemAction_FXXXXX" value="create"/>
                                <!-- Hidden Columns -->
                            </td>
                            <td>
			                    <select id="toTAIndex_FXXXXX" rel="to.select.index"  _isrequired name="To Index"><option value="" selected> Select Index</option></select>
                            </td>
                            <td>
			                    <select  id="toTAAccount_FXXXXX" rel="to.select.account" _isrequired name="To Account"><option selected value="">
			                    Select Account</option></select>
                            </td>
                            <td>
                                <span id="toTABalance_FXXXXX" rel="to.select.balance">$0.00</span>
                            </td>
                            <td>
                                <input type="text" class="form-control" id="toTAAmountToTransfer_FXXXXX" rel="to.select.amounttorecieve" placeholder="Amount" _isrequired name="Amount To Receive"/>
                            </td>
                        </tr>
                    </table>
	            </div>
	            <!-- Hidden Element -->

                <div class="form1 row setup-content" wizardpage="1">
                    <div class="col-md-12 well text-center">
                        <h3 class="text-center">STEP 1</h3>
                        <p>Enter Title and Budget Type</p>
                        <div class="container col-xs-12">
                            <div class="row clearfix">
                                <!-- below keeps the "next" button in the same well -->
                                <div class="col-md-12 column">
                                <table class="table table-bordered table-hover">
                                    <!-- thread groups elements together -->
                                    <thread>
                                    <tr>
                                        <th class="text-center">
                                        Title
                                        </th>
                                        <th class="text-center">
                                        Budget Type<span class="requiredFieldAsterisk">*</span>
                                        </th>
                                        <th class="text-center">
                                        Fiscal Year<span class="requiredFieldAsterisk">*</span>
                                        </th>
                                    </tr>
                                    </thread>
                                    <tbody>
                                    <td>
                                        <input type="text" id="txtBTR_title" name="title" rel-field="BTR.title" class="form-control" placeholder="Enter Title" />
                                    </td>
                                    <td>
                                        <div class="radio" requiredRadio>
                                            <label class="radio-inline">
                                                <input type="radio" name="optradio" id="budgetType_Permanent" rel-field="BTR.budgetType" value="Permanent" />Permanent
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="optradio" id="budgetType_Temporary" rel-field="BTR.budgetType" value="Temporary" />Temporary
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <select id="fiscalYear" rel-field="BTR.fiscalYear" name="fiscal year">
                                            <option selected="true">Select fiscal year</option>
                                        </select>
                                    </td>
                                    </tbody>
                                </table>
   		                        <div style="text-align:left"><label>Request By:</label><input type="text" id="txtBTR_Requestor" rel-field="BTR.Requester" disabled="true" /></div>
		                        <input type="hidden" id="txtBTR_Guid" />
		                        <input type="hidden" id="txtBTR_Id"/>
		                        <input type="hidden" id="txtBTR_ApprovedDate"/>
		                        <input type="hidden" id="txtBTR_life_cycle"/>
		                        <input type="hidden" id="txtBTR_internal_state"/>
		                        <input type="hidden" id="txtBTR_transfer_type"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form1 row setup-content" style="display:none" id="page2" wizardpage="2">
                    <div class="col-md-12 well text-center">
                        <h3 class="text-center"> STEP 2</h3>
                        <p>Complete Originating Department Information</p>
                        <div class="container col-xs-12">
                            <div class="row clearfix">
                                <div class="col-md-12 column" id="fromColumn">
                                    <table class="table table-bordered table-hover" >
                                        <thread>
                                        <tr>
                                            <th class="text-center">&nbsp;</th>
                                            <th class="text-center"><label>Index</label></th>
                                            <th class="text-center"><label>Account</label></th>
                                            <th class="text-center"><label>Available Balance</label></th>
                                            <th class="text-center"><label>Amount to Transfer Out ($):</label></th>
                                        </tr>
                                        </thread>
                                        <tbody datalabel="body"></tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5" style="text-align:right"><label style="font-style:oblique;padding-right:10px">Total</label><div style="float:right;padding-right:25px" id="totalFromValue">$0.00</div></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <div datalabel="footer">
                                        <div style="width:100%;text-align:left">
                                            <a datalabel="dlAddNewRow" class="btn btn-success">Add row</a>
                                        </div>
                                    </div>
                                </div>
			                </div>
                        </div>
                    </div>
                </div>
                <div class="form1 row setup-content" style="display:none" id="page3" wizardpage="3">
                    <div class="col-md-12 well text-center">
                        <h3 class="text-center"> STEP 3</h3>
                        <p>Complete Receiving Department Information</p>
                        <div class="container col-xs-12">
                            <div class="row clearfix">
                                <div class="col-md-12 column" id="toColumn">
                                    <table class="table table-bordered table-hover" >
                                        <thread>
                                        <tr>
                                            <th class="text-center">&nbsp;</th>
                                            <th class="text-center"><label>Index</label></th>
                                            <th class="text-center"><label>Account</label></th>
                                            <th class="text-center"><label>Current Balance</label></th>
                                            <th class="text-center"><label>Amount to Transfer In ($):</label></th>
                                        </tr>
                                        </thread>
                                        <tbody datalabel="body">
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5" style="text-align:right"><label style="font-style:oblique;padding-right:10px">Total</label><div style="float:right;padding-right:25px" id="totalToValue">$0.00</div></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <div datalabel="footer">
                                        <div style="width:100%;text-align:left">
                                            <a datalabel="dlAddNewRow" class="btn btn-success">Add row</a>
                                        </div>
                                    </div>
                                </div>
			                </div>
                        </div>
                    </div>
                </div>
                <div class="form1 row setup-content" style="display:none" id="page4" wizardpage="4">
                    <div class="col-md-12 well text-center">
                        <p>Enter Transfer activities</p>
                        <div class="container col-xs-12">
		                    <div class="row" style="padding-top:20px">
			                    <div class="col-md-12">
				                    <div style="margin-left:30px;display:inline;"><label>Explanation</label></div>
				                    <div style="display:inline;">
					                    <textarea name="Explanation" rows="2" cols="20" id="txtexplanation" style="height:100%;width:80%;" required></textarea>								
				                    </div>
			                    </div>
		                    </div>

		                    <div class="row" style="padding-top:25px;padding-bottom:10px;">
				                    <div class="col-md-1" style=""><label>Attachments</label></div>
				                    <div class="col-md-10" style="">
					                    <label for="file-upload" class="custom-file-upload">
						                    <i class="fa fa-cloud-upload"></i> Add Attachment (+)
					                    </label>
					                    <input id="file-upload" type="file" multiple onchange="DisplayPendingFiles('file-upload');"/>
                                        <div id="divPendingFiles" style="display:none" >
                                            <div>Upload(s):</div>
                                            <div id="divPendingfilesList"></div>
                                        </div>
                                        <div id="divSavedFiles" style="display:none">
                                            <div>Saved Files:</div>
                                            <div id="divSavedfileList"></div>
                                        </div>
				                    </div>
				                    <div class="col-md-1" style="">	</div>
		                    </div>

		                    <div class="row" style="padding-top:15px" >
				                    <div class="col-md-1" style="padding-bottom:25px"></div>
				                    <div class="col-md-10" style="">
					                    <div style="text-align:center">
                                            <a data-toggle="modal" onClick="SaveForm('draft'); return false;" class="btn btn-primary" style="width:90px">Save Draft</a>
                                            <a data-toggle="modal" onClick="SaveForm('submit'); return false;" class="btn btn-primary" style="width:90px">Submit</a>
                                          <!--  <a data-toggle="modal" onClick="CancelForm(false); return false;" class="btn btn-info" style="width:90px">Close</a> -->
                                            <a data-toggle="modal" onClick="CancelForm(false); return false;" class="btn btn-warning" style="width:90px">Cancel</a>
					                    </div>
				                    <div class="col-md-1" style="">	</div>
			                    </div>
		                    </div>
                        </div>
                    </div>
                </div>
                <div class="form1 row setup-content" style="display:none" id="page5" wizardpage="5">
		                <div class="row" style="padding-top:25px;display:none">
				                <div class="col-md-1" style=""></div>
				                <div class="col-md-10" style="">
					                <div style="text-align:center;font-weight:bolder;">
						                <span style="border-bottom:medium maroon solid">
						                For Budget Office Use Only</span>
					                </div>
				                <div class="col-md-1" style="">	</div>
			                </div>
		                </div>
		                <div class="row" style="padding-top:15px;display:none">
				                <div class="col-md-12" style="">
					                <table style="width:100%;border-collapse:separate;border-spacing:10px 5px" >
						                <tr>
							                <th style="color:#548dd4;border-bottom:medium black solid;text-align:center">
							                Approval History</th>
							                <th style="width:55%">Transfer Reason:</th>
							                <th>Rule Class</th>
							                <th>Budget JV#</th>
						                </tr>
						                <tr>
							                <td></td>
							                <td><textarea name="txtAreaBO" rows="2" cols="20" id="txtAreaBO" style="height:100%;width:100%;"></textarea></td>
							                <td><input type="text"></td>
							                <td><input type="text"></td>
						                </tr>
					                </table>
					                <div style="">
								
					                </div>
			                </div>
		                </div>
                </div>
            </form>
	        <div style="text-align:center">
		        <a id="previous-button" class="btn btn-warning pull-left" onclick="UI.Wizard.PreviousStep();" style="width:120px;padding-right:90px">Previous Step</a>&nbsp;&nbsp;
		        <a id="next-button" class="btn btn-primary pull-right" onclick="UI.Wizard.NextStep();" style="width:120px;">Next Step</a>
	        </div>
		    <div>
			    <div class="row" style="padding-top:25px;padding-bottom:35px">
					    <div class="col-md-1" style=""></div>
					    <div class="col-md-10">
						    <div id="divErrorPane" style="visibility:hidden;border:thin teal solid">
							    <div style="background-color:teal;color:white"><span style="font-weight:bold">
								    Form Error(s):</span></div>
							    <div id="divErrorBody" style="text-align:left"></div>
						    </div>
					    </div>
					    <div class="col-md-1" style="">	</div>
			    </div>
		    </div>
        </div>
        </div>
        <div class="col-md-1"></div>
    </div> 
    <!-- Ending DIV -->
</div>
<script src="../Scripts/wizard.js" type="text/javascript"></script>   
<script src="../Scripts/Page.BtrEdit.js" type="text/javascript"></script>
<script src="../Scripts/Banner.JV.js" type="text/javascript"></script>   
<script src="../Scripts/User.js" type="text/javascript"></script>
<script src="../Scripts/LookupValues.js" type="text/javascript"></script>
<script src="../Scripts/BudgetTransfer.js?reload=3" type="text/javascript"></script>
<script src="../Scripts/TransferActivity.js?reload=2" type="text/javascript"></script>
<script src="../Scripts/Index.js" type="text/javascript"></script>
<script src="../Scripts/Account.js" type="text/javascript"></script>   
<script src="../Scripts/waitdialog.js" type="text/javascript"></script>
</asp:content>