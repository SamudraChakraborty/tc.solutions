﻿<%@ Page language="C#" MasterPageFile="~site/_catalogs/masterpage/BTRApps.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>


<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
<!-- ================== BEGIN BASE CSS STYLE ================== -->

<!-- Optional theme -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css" />
<link rel="stylesheet" href="../Content/page_btredit.css" type="text/css"/> 
<link rel="stylesheet" href="../Content/main.css" type="text/css"/>
<link rel="stylesheet" href="../Content/hide_office_bar.css" type="text/css"/>

<!-- ================== END BASE CSS STYLE ================== -->
<style type="text/css">
.statusDivPareent {
	float:left;
}
.statusDiv, .statusDivText {
	font-size:medium;
	font-weight:bold;
	float:left;
	margin-right:20px

}
.buttons {
	text-transform:uppercase;
	background-color:#323232!important;
	font-weight:bolder;
	width:150px;
	color:white!important;
	font-size:14px;
	line-height:15px;
	border:none;
	margin-left:10px;
	margin-right:10px;
}

.glyphicon-ok {
	padding-left:5px;
	color:green
}

.glyphicon-flag {
	padding-left:5px;
	color:red
}
</style>
	
<!-- ================== BEGIN BASE JS ================== -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>   
<script src="../Scripts/fetch.js" type="text/javascript"></script> 
<script src="../Scripts/es6-promise.js" type="text/javascript"></script>
<script src="../Scripts/pnp.js" type="text/javascript"></script>
<script src="../Scripts/Utils.js" type="text/javascript"></script> <!-- this has to be after SharePoint JS files -->
<script src="../Scripts/BTR.Global.js" type="text/javascript"></script> 
<script src="../Scripts/EnvConfig.js" type="text/javascript"></script>
<script src="../Scripts/ApiConnector.js" type="text/javascript"></script> 
<script src="../Scripts/User.js" type="text/javascript"></script>
<script src="../Scripts/BudgetTransfer.js" type="text/javascript"></script>
<script src="../Scripts/TransferActivity.js" type="text/javascript"></script>
<script src="../Scripts/FileUtils.js" type="text/javascript"></script>
<script src="../Scripts/waitdialog.js" type="text/javascript"></script>

<!-- ================== END BASE JS ================== -->
	
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">BTR View Request Form</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
<div> <!-- Begining DIV -->


<!-- Page header  -->


<!-- Menu bar -->
<div class="container-fluid" style="padding-left:0;padding-right:0;display:none">
    <!-- Second navbar for profile settings -->
    
      <div class="row" >
      	<div class="col-lg-12">
      	<nav class="navbar navbar-default">
      		<ul class="nav navbar-nav navbar-left navbar-default">
            <!-- Menu Item -->
            <li><a href="BTR.aspx">Home</a></li>
          </ul>
      	</nav>
          
      	</div>
      </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
</div><!-- /.container-fluid -->
	
    <!-- My Requests -->
	<div class="row" >
		<div class="row">
		        <!-- begin panel -->
		    <div class="col-md-1"></div>
		    <div class="col-md-10"> 
		    <div style="width:100%" class="statusDivPareent" >
		    	<div id="divStatusNew"  class="statusDivText">NEW</div>
		    	<div class="statusDiv">-</div>
		    	<div id="divStatusDraft"  class="statusDivText">DRAFT</div>
		    	<div class="statusDiv">-</div>
		    	<div id="divStatusApproval" class="statusDivText">APPROVAL</div>
		    	<div class="statusDiv">-</div>
		    	<div id="divStatusReview" class="statusDivText">REVIEW</div>
		    	<div class="statusDiv">-</div>
		    	<div id="divStatusPosted" class="statusDivText">POSTED</div>
		    	</div>
		    </div>
		    <div class="col-md-1"></div>
		</div>
		<p/>
	</div>

    <div class="row" style="padding-top:15px">
    	<div class="col-lg-1"></div>
		<div class="col-lg-10" style="margin-left:2px">
			<div style="border-bottom:#337ab7 medium solid;color:black;font-size:16px">
				<span style="background-color:#337ab7;color:white;font-weight:bold;padding-left:5px;padding-right:5px">Budget Request Information</span>
			</div>
		</div>
    	<div class="col-lg-1"></div>
	</div>
	
    <div class="row" style="padding-top:10px;margin-left:5px;margin-right:5px">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div>
			<!-- TODO: Make title bigger -->
				<label>Budget #:&nbsp;</label><span id="txtBTR_Id"></span><br>
			<br/>
				<label>Title:&nbsp;</label><span id="txtBTR_Title"></span>
			</div>
			<div>
                <table>
                    <tr>
                        <td><label>Budget Type:</label></td><td><span id="budgetType"></span></td>
                    </tr>
                    <tr>
                        <td><label>Fiscal Year:&nbsp;</label></td><td><span id="fiscalYear"></span></td>
                    </tr>
                    <tr>
                        <td><label>Request By:</label></td><td><span id="txtBTR_Requestor" /></td>
                    </tr>
                    <tr>
                        <td><label>Modified by:&nbsp;</label></td>
                        <td><span id="modifiedby"></span></td>
                    </tr>
                    <tr>
                        <td><label>Modified:&nbsp;</label></td>
                        <td><span id="modifieddate"></span></td>
                    </tr>

                    <tr>
                        <td><label>Created by:&nbsp;</label></td>
                        <td><span id="createdby"></span></td>
                    </tr>
                    <tr>
                        <td><label>Created:&nbsp;</label></td>
                        <td><span id="createddate"></span></td>
                    </tr>
                    <tr>
                        <td><label>Saved Files:&nbsp;</label></td>
                        <td><div id="divSavedfileList"></div></td>
                    </tr>
                </table>
				<input type="hidden" id="txtBTR_Guid">
				<input type="hidden" id="txtBTR_LifeCycle">
				<input type="hidden" id="txtBTR_InternalState">
			</div>
            <div>
				<label>Explanation:&nbsp;</label><span id="explanation"></span>

            </div>
		</div>
		<div class="col-lg-1"></div>
	</div>
	
    <div class="row" style="padding-top:30px">
    	<div class="col-lg-1"></div>
		<div class="col-lg-10" style="margin-left:2px">
			<div style="border-bottom:#337ab7 medium solid;color:black;font-size:16px">
				<span style="background-color:#337ab7;color:white;font-weight:bold;padding-left:5px;padding-right:5px">
				Budget Transfer Activities</span>
			</div>
		</div>
    	<div class="col-lg-1"></div>
	</div>
	
    <!-- Transfer Activities -->
    
	<div class="row" style="padding-top:15px;margin-left:5px;margin-right:5px"> 
			<div class="col-md-1"></div>
			<div class="col-md-10"> 
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						</div>
						<h4 class="panel-title">Transfer Activities</h4>
					</div>
					<div class="table-responsive" id="divData">
						<table class='table table-striped table-td-valign-middle' id='dataTableTransferItems' >
							<thead>
								<tr class='primary' style="width:10%"><th>ID</th><th>
									ID</th><th>JV</th><th style="width:10%">JV 
									Status</th><th>Index</th><th>Acount</th><th>
									Amount</th>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
	</div>


	<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				&nbsp;
				<input type="button" value="Close" onclick="CloseScreen(); return false;"/> 
				&nbsp;
			</div>
			<div class="col-md-1"></div>
	</div>

</div> <!-- Ending DIV -->

<script type="text/javascript">

	function SetBTRForm(oItem)
	{
	
		$('#txtBTR_Title').html(oItem.title);
		$('#txtBTR_Id').html(oItem.btr_key);
		$("#lblRequester").html(oItem.requestor_uni_code);
		$("#txtBTR_Requestor").html(oItem.requestor_uni_code);
        $('#budgetType').html(oItem.budget_type);
        $('#fiscalYear').html(oItem.fiscal_year);
        $('#explanation').html(oItem.explanation);
        
		$("#divStatus"+ oItem.life_cycle).css('background-color','red').css('color','white');
		$('#createddate').html(oItem.created);		
		$('#createdby').html(oItem.created_by_name);		
		$('#modifiedby').html(oItem.modified_by_name);		
        $('#modifieddate').html(oItem.modified);
        
        RenderFiles(oItem.btr_guid);
    }	

	function JvMessage(oItem)
	{
		var message='';
		if (((typeof oItem.complete_jv_status_message) !== 'undefined') && (oItem.complete_jv_status_message !== null))
		{
			message += oItem.complete_jv_status_message;
			
		}
		if (((typeof oItem.create_jv_error_message) !== 'undefined' ) && (oItem.create_jv_error_message !== null))
		{
			message += ((message.length > 0) ? '<br/>' : '') + '(Err) - ' + oItem.create_jv_error_message.substring(1,10) + '...';
			
		}
		return message;
    }

    function RenderFiles(btr_guid) {
        FileUtils.GetFilesFromFolder(btr_guid,
            function (fileList) {
                if ((fileList.files == null) || (fileList.files.length == 0)) {
                    $("#divSavedfileList").html("No file(s)");
                    return;
                }
                debugger;
                var ulString = Array();
                ulString.push("<ul style='list-style-type: none'>");
                for (var index = 0; index < fileList.files.length; index++) {
                    ulString.push("<li><a href=''>" + fileList.files[index].name + "</a></li>");
                }
                ulString.push("</ul>");
                $("#divSavedfileList").html(ulString.join(''));
            }
        );

    }
	
	function JvDocId(oItem)
	{
		return ((oItem.jv_doc_id !== null) ? oItem.jv_doc_id : '')  + ((oItem.jv_complete == 1) ? '<span class="glyphicon glyphicon-ok">' : '') + ((oItem.create_jv_error_message !== null) ? '<span class="glyphicon glyphicon-flag">' : '');
	}


	function PopulateTransferActivityTable(transferActivities,tableObj) {

		if (transferActivities.length == 0) return;
		for (iCount=0; iCount < transferActivities.length; iCount ++) {
			
			oItem = transferActivities[iCount];
			if (oItem.position_type.toLowerCase() == "from")
			{
				tableObj.row.add([oItem.position_type,
									oItem.transfer_activity_key , //Item Guid
									JvDocId(oItem),
									JvMessage(oItem),
									oItem.index_number_description,
									oItem.account_number_description,
									Utils.Currency.format(oItem.amount)
								] ).draw( true );
			}

		}
		for (iCount=0; iCount < transferActivities.length; iCount ++) {
			
			oItem = transferActivities[iCount];
			if (oItem.position_type.toLowerCase() == "to")
			{
				//debugger;
				tableObj.row.add([
									oItem.position_type,
									oItem.transfer_activity_key, //Item Guid
									JvDocId(oItem),
									JvMessage(oItem),
									oItem.index_number_description,
									oItem.account_number_description,
                                    Utils.Currency.format(oItem.amount)
								] ).draw( false );
			}

		}
	}
	
	function GetBudgetAndTransferActivities(btrID,tableObj)
	{
		BTR.BudgetTransfer.Item(btrID,
			function(btrObj) {
				if (btrObj == null) throw "Budget transfer request could not be found ("+btrID+")";
				SetBTRForm(btrObj);
					
				BTR.TransferActivity.GetBTRAssociatedTransferActivities(_gForm_ID, 
					function(transferActivities) {
						PopulateTransferActivityTable(transferActivities,tableObj);
					},
					function(error) {
                        console.error('Error getting transfer activities objects.');
                        throw error;
					}
				);
			},
			function (data) {
                console.error("Error getting the budget transfer request object: " + data);
                throw data;
			}
		);
	}

    function LoadDataGrid() {
        try {
            _gDataTableTransferItems = $('#dataTableTransferItems').DataTable({
                "columnDefs": [{
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                }],
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;

                    api.column(0, { page: 'current' }).data().each(function (group, i) {
                        if (last != group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5" style="font-weight:bold">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });

        } catch (err) {
            console.error(err);
            throw err;
        }
	}

	function CancelScreen() {
		window.location.href = "../";
	}
	
	function CloseScreen() {
		window.location.href = "../";
	}

    function PageStart() {

        BTR.BudgetTransfer.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);
        BTR.TransferActivity.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);

        BTR.User.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);

        BTR.User.GetCurrent(_gUserLoginName,
            function () {
                $("#userField").html(BTR.User.UserField());
                LoadDataGrid();
                GetBudgetAndTransferActivities(_gForm_ID, _gDataTableTransferItems); 
                
                waitingDialog.hide();
            },
            function (err) {
                console.error('Error loading form'+err);
                waitingDialog.hide();
                Logger.logit("ERROR", "BTRReview.aspx", 1, "Page_Start", err);
            }
        );
    }
//Global Variables
	var _gUserLoginName = _spPageContextInfo.userLoginName;
	var _gForm_ID = Utils.GetParamByName("rID");
	var _gDataTableTransferItems = null;
	var _gJsonTransferItems = [];
	
	
    //starting point for execution of javascript code for the page
    $(document).ready(function () {
        waitingDialog.show();
        try {
            BTR.Global.PageStart = function () {
                PageStart();
            };
            BTR.Global.Initialize(); //initialize global variables
        }
        catch (err) {
            waitingDialog.hide();
            Logger.logit("ERROR", "BTRReview.aspx", 1, "Page_Document_ready", null);
        }
    });
</script>
</asp:Content>