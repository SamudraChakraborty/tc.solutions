﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
    <SharePoint:ScriptLink name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <meta name="WebPartPageExpansion" content="full" />

    <!-- Add your CSS styles to the following file -->

    <!-- Add your JavaScript to the following file -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../Scripts/Utils.js" type="text/javascript"></script> <!-- this has to be after SharePoint JS files -->
<script src="../Scripts/ApiConnector.js" type="text/javascript"></script> 
<script src="https://secure.aadcdn.microsoftonline-p.com/lib/1.0.15/js/adal.min.js" type="text/javascript"></script>
<script type="text/javascript">
    "use strict";

    var BTR = BTR || {};
    BTR.Values = Object.create(BTR.ApiConnector);  //inherit from the ApiConnector object;
    BTR.Values.ListBaseApiUrl = "/api/Values/";


    var BTR = BTR || {};
    BTR.OAuth = {};

    BTR.OAuth.clientId = 'edc0e656-8a20-40a4-8e10-cc47ba65da64';
    BTR.OAuth.tenant = '27981752-a6d1-4f09-be89-de5e51ae7180';

    BTR.OAuth.config = {
        tenant: BTR.OAuth.tenant,
        clientId: BTR.OAuth.clientId,
      endpoints: {
                    graphUri: 'https://graph.microsoft.com'
      },
      cacheLocation: "localStorage"
  };
  //Create the autentication context
    BTR.OAuth.authContext = new AuthenticationContext(BTR.OAuth.config);

    BTR.OAuth.getAuthToken = function(endpoint){
      var d = jQuery.Deferred();

      //Read the token from the cache
      var tokenCache = BTR.OAuth.authContext.getCachedToken(endpoint);

      if(tokenCache == undefined) {
                    //If token is undefined, then call AAD to get a new token
          BTR.OAuth.authContext.acquireToken(endpoint, function (error, token) {
                        if (error || !token) {
                            d.reject(error);
                        }
                        else {
                            d.resolve(token);
                        }
                    });
                }else{
                    d.resolve(tokenCache);
                }
      //Return a promise for acquiring token
      return d.promise();
  };

    BTR.OAuth.getGraphData = function(){
      //Get the token, either from the cache or from the server
        var tokenPromise = BTR.OAuth.getAuthToken(BTR.OAuth.config.endpoints.graphUri);
      tokenPromise.then(function(token){
          //Promise for token resolved
          if(token != undefined) {
              //Valid token, make a REST call to the MSGraphAPI
              var meUri = "https://graph.microsoft.com/v1.0/me";
              $.ajax
              ({
                    type: "GET",
                  url: meUri,
                  headers: {
                    //Include the token
                    "Authorization": "Bearer " + token
                  }
              }).done(function (response) {
                    $("#message").text("Got the data.");
              }).fail(function () {
                    $("#message").text("Failed to get the data.");
                });
          }
      }, function(error){console.log(JSON.stringify(error));});
  };

  $(document).ready(function() {
      // Check For & Handle Redirect From AAD After Login or Acquiring Token
      var isCallback = BTR.OAuth.authContext.isCallback(window.location.hash);

      if (isCallback && !BTR.OAuth.authContext.getLoginError()) {
          BTR.OAuth.authContext.handleWindowCallback(window.location.hash);
                }else{
          var user = BTR.OAuth.authContext.getCachedUser();
          if (!user) {
                    //Log in user
              BTR.OAuth.authContext.login();
                }else{
              BTR.OAuth.getGraphData();
          }
      }
 /*
        BTR.Values.SiteUrl('https://btrdev-kj.azurewebsites.net');
        BTR.Values.ExecuteApiGet('1', null,
            function (data) {
                debugger;
                console.log(data);
            },
            function (err) {
                debugger;
                console.log(err);
        });

      */
  });
</script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Page Title
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div>
        <p id="message">            initializing...        </p>

    </div>

</asp:Content>
