﻿<%@ Page language="C#" MasterPageFile="~site/_catalogs/masterpage/BTRApps.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">

<!-- ================== BEGIN BASE CSS STYLE ================== -->

<!-- Optional theme -->
<link rel="stylesheet" href="../Content/main.css" type="text/css"/>
<link rel="stylesheet" href="../Content/hide_office_bar.css" type="text/css"/>


<!-- ================== END BASE CSS STYLE ================== -->
	
<!-- ================== BEGIN BASE JS ================== -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>   
<script src="../Scripts/fetch.js" type="text/javascript"></script> 
<script src="../Scripts/es6-promise.js" type="text/javascript"></script>
<script src="../Scripts/pnp.js" type="text/javascript"></script>
<script src="../Scripts/AppInitializer.js" type="text/javascript"></script> 
<script src="../Scripts/EnvConfig.js" type="text/javascript"></script>
<script src="../Scripts/Utils.js" type="text/javascript"></script>
<script src="../Scripts/FileUtils.js" type="text/javascript"></script>


<!-- ================== END BASE JS ================== -->
	
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">BTR Approval Request Form</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">

    		                    <div class="row" style="padding-top:25px;padding-bottom:10px;">
				                    <div class="col-md-1" style=""><label>Attachments</label></div>
				                    <div class="col-md-10" style="">
					                    <label for="file-upload" class="custom-file-upload">
						                    <i class="fa fa-cloud-upload"></i> Add Attachment (+)
					                    </label>
					                    <input id="file-upload" type="file" multiple/>
				                    </div>
				                    <div class="col-md-1" style="">	</div>
		                    </div>
    <input type="text" id="webUrl" value="/sites/Developers/BT/BtrSP/Lists/BTRDocs/" /><br />
    Folder Name:<input type="text" id="folderName" value="" />
    <div id="messages">loading...</div>
			<input type="button" value="Upload Files" onclick="FileUtils.UploadFiles('file-upload', '11988df0-8622-11e7-9598-0800200c9a66'); return false;"/> 
			<input type="button" value="Create Folder" onclick="FileUtils.CreateFolder('file-upload'); return false;"/> 
<input type="button" value="Folder Exists True" onclick="FileUtils.FolderExists('testing'); return false;"/> 
<input type="button" value="Files from folder" onclick="FileUtils.GetFilesFromFolder('folderName'); return false;"/> 

			<input type="button" value="Upload Web" onclick="HostWebLists('file-upload'); return false;"/> 


</asp:Content>