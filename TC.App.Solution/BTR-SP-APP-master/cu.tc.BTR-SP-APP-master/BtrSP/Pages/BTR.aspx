﻿<%@ Page language="C#" MasterPageFile="~site/_catalogs/masterpage/BTRApps.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">

<!-- ================== BEGIN BASE CSS STYLE ================== -->
    
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css"/>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css"/>


<link rel="stylesheet" href="../Content/page_btr.css"/>
<link rel="stylesheet" href="../Content/hide_office_bar.css" type="text/css"/>
<link rel="stylesheet" href="../Content/main.css" type="text/css"/>

<!-- ================== END BASE CSS STYLE ================== -->
	
<!-- ================== BEGIN BASE JS ================== -->
<SharePoint:ScriptLink name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
<SharePoint:ScriptLink name="sp.runtime.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>    
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js" type="text/javascript"></script> 
<script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js" type="text/javascript"></script> 
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js" type="text/javascript"></script>    
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js" type="text/javascript"></script>    
<script src="../Scripts/fetch.js" type="text/javascript"></script> 
<script src="../Scripts/es6-promise.js" type="text/javascript"></script>
<script src="../Scripts/pnp.js" type="text/javascript"></script>
<script src="../Scripts/Utils.js" type="text/javascript"></script> <!-- this has to be after SharePoint JS files -->
<script src="../Scripts/BTR.Global.js" type="text/javascript"></script> 
<script src="../Scripts/EnvConfig.js" type="text/javascript"></script>
<script src="../Scripts/ApiConnector.js" type="text/javascript"></script> 
<script src="../Scripts/Logger.js" type="text/javascript"></script>
<script src="../Scripts/User.js" type="text/javascript"></script>
<script src="../Scripts/BudgetTransfer.js" type="text/javascript"></script>
<script src="../Scripts/TransferActivity.js" type="text/javascript"></script>
<script src="../Scripts/FileUtils.js" type="text/javascript"></script>
<script src="../Scripts/waitdialog.js" type="text/javascript"></script>


<!-- ================== END BASE JS ================== -->
	
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">BTR Home</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
<div>



<!-- Toggle Buttons  -->

<div class="row" >
        <div class="col-md-1"></div>
        <div class="col-md-10">
        	<div class="row">
		        <div class="col-md-3 col-xs-3">
		            <!-- begin widget -->
		            <div class="widget widget-stat bg-green widget-stat-right text-white" onclick="ShowAll();">
		                <div class="widget-stat-icon" ><span class="glyphicon glyphicon-file"></span></div>
		                <div class="widget-stat-info hidden-xs">
		                    <div class="widget-stat-title" >All</div>
		                    <div class="widget-stat-number" id="divStatsAllText"></div>
		                </div>
		            </div>
		            <!-- end widget -->
		        </div>
		        <!-- begin col-3 -->
		        <div class="col-md-3 col-xs-3">
		            <!-- begin widget -->
		            <div class="widget widget-stat bg-green widget-stat-right text-white" onclick="HideOrShow('Draft');">
		                <div class="widget-stat-icon" ><span class="glyphicon glyphicon-file"></span></div>
		                <div class="widget-stat-info  hidden-xs">
		                    <div class="widget-stat-title" >Draft Requests</div>
		                    <div class="widget-stat-number" id="divStatsRequestsText"></div>
		                </div>
		            </div>
		            <!-- end widget -->
		        </div>
		        <!-- end col-3 -->
		        <!-- begin col-3 -->
		        <div class="col-md-3 col-xs-3">
		            <!-- begin widget -->
		            <div class="widget widget-stat widget-stat-right text-white bg-blue" onclick="HideOrShow('Transactions');">
		                <div class="widget-stat-icon" ><span class="glyphicon glyphicon-road"></span></div>
		                <div class="widget-stat-info  hidden-xs">
		                    <div class="widget-stat-title" >Approval Queue</div>
		            		<div class="widget-stat-number" id="divStatsTransferRequestsText">0</div>
		
		                </div>
		            </div>
		            <!-- end widget -->
		        </div>
			    <!-- end col-3 -->
		        <!-- begin col-3 -->
		        <div class="col-md-3 col-xs-3">
		            <!-- begin widget -->
		            <div class="widget widget-stat widget-stat-right text-white bg-info" onclick="HideOrShow('Approvals');">
		                <div class="widget-stat-icon" ><span class="glyphicon glyphicon-list-alt"></span></div>
		                <div class="widget-stat-info  hidden-xs">
		                    <div class="widget-stat-title" >Transaction History</div>
		                    <div class="widget-stat-number" id="divStatsRequestApprovalsText"></div>
		                </div>
		            </div>
		            <!-- end widget -->
		        </div>
		        <!-- end col-3 -->
        	</div> <!-- end of row -->
        </div> <!-- end of col 10 -->
        <div class="col-md-1"></div>
</div>

<!-- Data Tables  -->
		
<p/>
    <!-- Requests -->
	<div class="row tbls" id="tblDraft">
	
				<!-- begin panel -->
			<div class="col-md-1"></div>
			<div class="col-md-10">            
				<div class="panel panel-primary">
					<div class="panel-heading" >
						<div class="panel-heading-btn">
						</div>
						<h4 class="panel-title" style="float:none">Draft Requests</h4>
					</div>
					<div class="table-responsive" id="divDataRequests">
					<table class='table table-striped table-td-valign-middle table-bordered dt-responsive nowrap' cellspacing="0" width="100%" id='dtTransferDrafts' >
						<thead>
							<tr class='primary'>
                            <th width="50px">*</th>
							<th width="100px">ID</th>
							<th width="200px">Title</th>
							<th width="200px">Budget Type</th>
							<th width="200px">Transfer Amount</th>
							<th width="200px">Modified</th>
							<th width="200px">Modified By</th></tr>
						</thead>
						<tbody></tbody>
					</table>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
	</div>
	
		<!-- Pending to Banner -->
	<div class="row tbls" id="tblBanner" style="display:none">
				<!-- begin panel -->

			<div class="col-md-1"></div>
			<div class="col-md-10">            
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-expand"><i class="fa fa-expand"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-remove"><i class="fa fa-times"></i></a>
						</div>
						<h4 class="panel-title">Banner Submission</h4>
					</div>

					<div class="table-responsive" id="divDataBanner">
					
					
					<table class='table table-striped table-td-valign-middle table-bordered dt-responsive nowrap' cellspacing="0" width="100%" id='dtBannerPending' >
						<thead>
							<tr class='primary'>
								<th>ID</th><th>Modified</th>
								<th>Type</th><th>Index</th>
								<th>Account</th><th>Amount</th><th>Stage</th>
								<th>Approver</th><th>Approval</th>
							</tr>
						</thead>
					</table>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>

	</div>

		<!-- Transfer Transactions -->
	<div class="row tbls" id="tblTransactions">
				<!-- begin panel -->

			<div class="col-md-1"></div>
			<div class="col-md-10">            
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-expand"><i class="fa fa-expand"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-remove"><i class="fa fa-times"></i></a>
						</div>
						<h4 class="panel-title">Approval Queue</h4>
					</div>

					<div class="table-responsive" id="divData">
					
					
					<table class='table table-striped table-td-valign-middle table-bordered dt-responsive nowrap' cellspacing="0" width="100%" id='dtTransactions' >
						<thead>
							<tr class='primary'>
								<th>ID</th><th>Modified</th>
								<th>Type</th><th>Index</th>
								<th>Account</th><th>Amount</th><th>Stage</th>
								<th>Approver</th><th>Approval</th>
							</tr>
						</thead>
					</table>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>

	</div>
	
	    <!-- Approvals -->
	<div class="row tbls" id="tblApprovals">
				<!-- begin panel -->
			<div class="col-md-1"></div>
			<div class="col-md-10">            
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="panel-heading-btn">
							<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
						</div>
						<h4 class="panel-title">Transaction History</h4>
					</div>

					<div class="table-responsive" id="divDataRequests3">
					
					<!-- <table id='dataTableApprovals' class='table table-striped table-bordered dt-responsive nowrap table-td-valign-middle' cellspacing="0"  width="100%" > -->
					 <table id='dtTransferApprovals' class="table table-striped table-bordered table-td-valign-middle  table-bordered dt-responsive nowrap" cellspacing="0" width="100%" cellspacing="0"  width="100%" >
						<thead>
							<tr class='primary'>
                            <th width="50px">*</th>
							<th style="width:50px">ID</th>
							<th>Title</th>
                            
							<th>Stage</th>
							<th>Budget Type</th>
							<th>JV Doc ID</th>
							<th>JV Status</th>
							<th>Transfer Amount</th>
							<th>Modified</th>
							<th>Modified By</th></tr>
						</thead>
						<tbody></tbody>
					</table>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
	</div>
    <div style="display:none">
        <div id="toolbar_drafts_hidden">
            <a onclick="DeleteCheckedRows('#dtTransferDrafts'); return false;"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span></a>
            <a onclick="showChecked(); return false;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
        </div>

    </div>
</div>

<script type="text/javascript">
    function DeleteCheckedRows(tableId) {
        var selected = tableId + ' .selected';
        var n = $(selected).length;
        alert('Checked' + n);
    }

	function HideOrShow(title)
	{
		$('.tbls').hide();
		$('#tbl'+title).show();
	}

    function ShowAll() {
		$('.tbls').show();
	}

	function UpdateTransferPriority(thisRef, btrId, eTag, currentValue)
	{
		var priorityFlag = !(currentValue == true); //this will handle nulls
		
		eTag = "*"; //quick fix for now
		var jsonData = BTR.BudgetTransfer.DataItem.Create();
		jsonData['priority_flag'] = priorityFlag ;	
		
		BTR.BudgetTransfer.Update(btrId,jsonData,eTag,
			function(data) {
				//GetTransactions();
				//toggle images instead of doing a data refresh
				priorityIcon = (priorityFlag) ? "star_selected.png" : "star_unselected.png";
				thisRef.childNodes[0].src = "../images/"+priorityIcon;
			},
			function(errData) {
                console.error(errData);
                console.error("Error: Updating Transaction "+listItemId);
			}
		);

	}
	
	function UpdateTransferActivityApprovalStatus(transfer_activity_key,action)
	{
		eTag = "*"; //quick fix for now
		var vActionValue = (action==1) ? "Approved":"Rejected";
		var jsonData = BTR.TransactionActivity.DataItem.Create();
		jsonData['ApprovalStatus'] = vActionValue;	
		
		BTR.TransactionActivity.Update(transfer_activity_key,jsonData,
			function(data) {
				//GetTransactions();
			},
			function(errData) {
                console.error(errData);
                console.error("Error: Updating Transfer Activity ( "+transfer_activity_key +")");
			}
		);
	}
	
	function CountAll()
	{
		var val1 = Number($('#divStatsTransferRequestsText').text());
		var val2 = Number($('#divStatsRequestsText').text());
		var val3 = Number($('#divStatsRequestApprovalsText').text());		
		
		$('#divStatsAllText').text(val1 + val2 + val3);
	}

	function PopulateTableTransferActivities(data) {

		var count = data.length;
		$('#divStatsTransferRequestsText').text(count);
		
		_gTableTransferActivities.clear();
		$.each(data, function() {
			if ((this.approval_status_code== 'A') || (this.approval_status_code == 'R'))
			{
				btnUpdateStatus = '';
			}
			else
			{
				btnUpdateStatus = "<span><a href='#' class='ButtonApprove' onclick='UpdateTransferActivityApprovalStatus("+this.ID+",1); return false;'>Approve</a> <a href='#' class='ButtonReject' onclick='UpdateTransferActivityApprovalStatus("+this.ID+",0); return false;'>Reject</a></span>";
			}
			_gTableTransferActivities.row.add([
				this.btr_key+'-'+this.transfer_activity_key,
                Utils.DateFormat(this.modified),
				this.position_type ,						
				this.index_number_description,
				this.account_number_description,
                Utils.Currency.format(this.amount),
				this.approval_status_key,
				this.approver_email,
				btnUpdateStatus
			] ).draw( false );					
		});
	}
	
	function GetTransferActivities()
    {
        return;
        BTR.TransferActivity.ReviewerOwned(BTR.User.Current.uni_key, 
			function (data) {
				PopulateTableTransferActivities(data);
			},
			function (data) {
                console.error("Error(Getting Transfer Activities): "+ data.Message + '\n'+data.InnerError );
			}
		);
	}

    function PopulateApprovalTasks()
    {
        var listName = '';
        return; //code is not ready yet
        $pnp.sp.web.lists.getByTitle("WorkflowTaskList").items.get().then(r => {
            //debugger;
            var key;
            var value;
            for (var index = 0; index < r.length; index++) {
                value = r[index].value.toLowerCase();
                key = r[index].Title.toLowerCase();
                consol.log('workflow tasks'+key);
            }
        }).catch(function (err) {
            console.error(err);
        });

    }
	function PopulateTableBudgetTransferRequests(data) {
		var countTotal = data.length;
		var countRequest = 0;
		//var siteUrl = _spPageContextInfo.webAbsoluteUrl;

        //location.search;
        var editUrl = 'BTREdit.aspx?';
        var editUrl2 = 'BTRReview.aspx?';	
        var sourceUrl = "&" + location.search//+encodeURIComponent(siteUrl + '/Pages/BTR.aspx'); //TODO: fix this
		
		var prorityIcon;
		var transferType;
		var pageUrl;
		
		$.each(data, function() {
		
			prorityIcon = ((this.priority_flag !== null) && (this.priority_flag)) ? "star_selected.png" : "star_unselected.png";
			pageUrl = "rID=" + this.btr_key + "&transfertype=" + this.transfer_type.toLowerCase() + sourceUrl;

			if (this.life_cycle.valueOf().toLowerCase() == "draft")
			{
				countRequest++;
                _gTableBTR_Drafts.row.add([
                    "",
				"<a href='"+editUrl + pageUrl+"'><img src='../images/Black_Pencil_Icon.png'/></a>&nbsp;" + 
				"<a href='#' onclick='UpdateTransferPriority(this,"+this.btr_key+",*,"+this.priority_flag +"); return false;'><img src='../images/"+prorityIcon+"'/></a>&nbsp;" +
                this.btr_key,
				this.title,
				this.budget_type,
                Utils.Currency.format(this.total_amount),
				Utils.DateFormat(this.modified),
				this.modified_by_name
				] ).draw( false );	
			}
			else {	
                _gTableBTR_Approvals.row.add([
                    "",
				"<a href='"+editUrl2 + pageUrl + "'><img src='../images/Black_Pencil_Icon.png'/></a>&nbsp;" +	
				"<a href='#' onclick='UpdateTransferPriority(this,"+this.btr_key+",*,"+this.priority_flag+"); return false;'><img src='../images/"+prorityIcon+"'/></a>&nbsp;" +
                    this.btr_key,
				this.title,
				this.life_cycle + "<span class='halflings halflings-adjust'></span> ",
				this.budget_type,
				this.jv_doc_id,
				this.jv_status_code,
                Utils.Currency.format(this.total_amount),
				Utils.DateFormat(this.modified),
				this.modified_by_name
				] ).draw( false );	
			}
		});
		$('#divStatsRequestsText').text(countRequest);
		$('#divStatsRequestApprovalsText').text(countTotal-countRequest);					
	}
	
	function GetBudgetTransferRequests()
    {
        BTR.BudgetTransfer.ItemTableRecord(BTR.User.Current.uni_key, 
			function (data) {
				PopulateTableBudgetTransferRequests(data);
				CountAll();
			}, 
			function (data) {
                console.error("Error(Getting Transfer Requests): "+ data.Message + '\n'+data.InnerError );
			}
		);
	}
    function ConfigureDataTables() {
        _gTableBTR_Drafts = $('#dtTransferDrafts').DataTable({
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            order: [[1, 'asc']]
        });
        _gTableBTR_Approvals = $('#dtTransferApprovals').DataTable({
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            order: [[1, 'asc']]
        });

        var toolbarDrafts = $('#toolbar_drafts_hidden').html();
        toolbarDrafts.replace('_hidden', '');
        $("div.toolbar_drafts").html(toolbarDrafts);
        $("div.toolbar_approvals").html('');
        _gTableTransferActivities = $('#dtTransactions').DataTable();

    }
    function PageStart()
    {
        try {
            _gHostWebUrl = Utils.GetParamByName('SPHostUrl', document.URL);
            _gAppWebUrl = Utils.GetParamByName('SPAppWebUrl', document.URL);

            ConfigureDataTables();
            BTR.BudgetTransfer.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);
            BTR.TransferActivity.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);
            BTR.User.SiteUrl(GlobalConfig.BTR.Rest_Api_Url);

            BTR.User.GetCurrent(_gLoginName,
                function () {
                    $("#userField").html(BTR.User.UserField());
                    GetBudgetTransferRequests();
                    GetTransferActivities();
                    PopulateApprovalTasks();
                    waitingDialog.hide();
                },
                function () {
                    console.error('Error loading form');
                    waitingDialog.hide();
                    Logger.logit("ERROR", "BTR.aspx", 1, "Get Current User", null);
                }
            );

        } catch (err) {
            waitingDialog.hide();
            Logger.logit("ERROR", "BTR.aspx", 1, "Page_Start", err);
        }
    }

	var _gLoginName = _spPageContextInfo.userLoginName;
	var _gUni = Utils.GetUniValue(_gLoginName);
	var _gTableBTR_Drafts = null;
	var _gTableBTR_Approvals = null;
	var _gTableTransferActivities = null;
    var _gHostWebUrl = null;
    var _gAppWebUrl = null;

    //starting point for execution of javascript code for the page
    $(document).ready(function () {
        waitingDialog.show();
        try {
            BTR.Global.PageStart = function () {
                PageStart();
            };
            BTR.Global.Initialize(); //initialize global variables
        }
        catch (err) {
            waitingDialog.hide();
            Logger.logit("ERROR", "BTR.aspx", 1, "Page_Document_ready", null);
        }
	});
</script>
</asp:Content>