﻿<%@ Page language="C#" MasterPageFile="~site/_catalogs/masterpage/BTRApps.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">

<!-- ================== BEGIN BASE CSS STYLE ================== -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/6.0.0/normalize.min.css"/>
<link rel="stylesheet" href="../Content/page_btr.css"/>
<link rel="stylesheet" href="../Content/hide_office_bar.css" type="text/css"/>
<link rel="stylesheet" href="../Content/main.css" type="text/css"/>

<!-- ================== END BASE CSS STYLE ================== -->
	
<!-- ================== BEGIN BASE JS ================== -->
<SharePoint:ScriptLink name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../Scripts/fetch.js" type="text/javascript"></script> 
<script src="../Scripts/es6-promise.js" type="text/javascript"></script>
<script src="../Scripts/pnp.js" type="text/javascript"></script>
<script src="../Scripts/EnvConfig.js" type="text/javascript"></script>


<!-- ================== END BASE JS ================== -->
	
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">BTR Admin</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
    <ul>
        <li><asp:HyperLink runat="server" NavigateUrl="JavaScript:window.location = _spPageContextInfo.webAbsoluteUrl + '/Lists/WorkflowQueueBTR/AllItems.aspx';" Text="WorkflowQueueBTR" /></li>
        <li><asp:HyperLink runat="server" NavigateUrl="JavaScript:window.location = _spPageContextInfo.webAbsoluteUrl + '/Lists/Configurations/AllItems.aspx';" Text="Configurations" /></li>
        <li><asp:HyperLink runat="server" NavigateUrl="JavaScript:window.location = _spPageContextInfo.webAbsoluteUrl + '/Lists/wfBTR/AllItems.aspx';" Text="BTR Workflow" /></li>
        <li><asp:HyperLink runat="server" NavigateUrl="JavaScript:window.location = 'TestUpload.aspx?'+window.location.search.slice(1);" Text="Test Upload" /></li>
    </ul>
    <br />
    <div class="row tbls" id="tblDraft">
	
			<div class="col-md-1"></div>
			<div class="col-md-10">            
				<div class="panel panel-primary">
					<div class="panel-heading" >
						<div class="panel-heading-btn">
						</div>
						<h4 class="panel-title" style="float:none">Transaction Requiring Approval</h4>
					</div>
					<div class="table-responsive" id="divDataRequests">
					<table class='table table-striped table-td-valign-middle table-bordered dt-responsive nowrap' cellspacing="0" width="100%" id='dtTransferDrafts' >
						<thead>
							<tr class='primary'>
                            <th width="50px">*</th>
							<th width="100px">ID</th>
							<th width="200px">Title</th>
							<th width="200px">Budget Type</th>
							<th width="200px">Transfer Amount</th>
							<th width="200px">Modified</th>
							<th width="200px">Modified By</th></tr>
						</thead>
						<tbody>
                            <td valign="top" colspan="8" class="dataTables_empty">No data available in table</td>
						</tbody>
					</table>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
	</div>
    
    <div class="row tbls" id="tblDraft">
	
			<div class="col-md-1"></div>
			<div class="col-md-10">            
				<div class="panel panel-primary">
					<div class="panel-heading" >
						<div class="panel-heading-btn">
						</div>
						<h4 class="panel-title" style="float:none">Not Able To Process</h4>
					</div>
					<div class="table-responsive" id="divDataRequests">
					<table class='table table-striped table-td-valign-middle table-bordered dt-responsive nowrap' cellspacing="0" width="100%" id='dtTransferDrafts' >
						<thead>
							<tr class='primary'>
                            <th width="50px">*</th>
							<th width="100px">ID</th>
							<th width="200px">Title</th>
							<th width="200px">Budget Type</th>
							<th width="200px">Transfer Amount</th>
							<th width="200px">Modified</th>
							<th width="200px">Modified By</th></tr>
						</thead>
						<tbody>
                            <td valign="top" colspan="8" class="dataTables_empty">No data available in table</td>
						</tbody>
					</table>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
	</div>
    <div style="display:none">
        <div id="toolbar_drafts_hidden">
            <a onclick="DeleteCheckedRows('#dtTransferDrafts'); return false;"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span></a>
            <a onclick="showChecked(); return false;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
        </div>

    </div>
</asp:Content>